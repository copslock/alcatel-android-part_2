package com.tct.android.tools.feature.plf2fq;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.tct.android.tools.feature.app.Main;

public class PlfParser {
    private static final String KEY_VAR       = "VAR";
    private static final String KEY_SDMID     = "SDMID";
    private static final String KEY_METATYPE  = "METATYPE";
    private static final String KEY_VALUE     = "VALUE";

    public synchronized static void parse(String file, final HashMap<String, FqJava.Field> fields) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            InputStream is = new FileInputStream(file);
            SAXParser parser = factory.newSAXParser();
            parser.parse(is, new DefaultHandler() {
                private boolean inSdm;
                private String tag;
                private FqJava.Field field;
                private String keySdmid = null;
                private String plfType = null;

                @Override
                public void startElement(String uri, String localName,
                        String name, Attributes atrbts) throws SAXException {
                    super.startElement(uri, localName, name, atrbts);
                    if (name.equals(KEY_VAR)) {
                        inSdm = true;
                        field = new FqJava.Field();
                    }
                    tag = name;
                }

                @Override
                public void characters(char[] chars, int i, int i1)
                        throws SAXException {
                    super.characters(chars, i, i1);
                    String value = new String(chars, i, i1);
                    if (inSdm) {
                        if (tag.equals(KEY_SDMID)) {
                            keySdmid = value;
                            field.name = keySdmid;
                            return;
                        } 
                        if (tag.equals(KEY_METATYPE)) {
                            plfType = value.split(",")[0].trim();
                            field.type = plfType;//NEED to keep plftype.Perso.convertType(plfType);
                            return;
                        }
                        if (tag.equals(KEY_VALUE)) {
                            field.value = Perso.convertValue(plfType, value);
                            return;
                        }
                    } 
                }

                @Override
                public void endElement(String uri, String localName, String name)
                        throws SAXException {
                    super.endElement(uri, localName, name);
                    if (name.equals(KEY_VAR)) {
                        inSdm = false;
                        if (keySdmid != null) {
                            if (fields.containsKey(keySdmid)) {
                                Main.LOG.w("SAME SDMID in both plf and xplf. SDMID=" + keySdmid);
                            } else {
                                fields.put(keySdmid, field);
                            }
                            keySdmid = null;
                            field = null;
                            plfType = null;
                        }
                    }
                    tag = "";
                }
            });
        } catch (Exception ex) {
            Main.LOG.e("PlfParser Error:" + ex);
        }
    }
}
