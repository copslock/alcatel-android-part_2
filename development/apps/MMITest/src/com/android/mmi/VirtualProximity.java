/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Proximity.java     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.TextView;

public class VirtualProximity extends TestBase implements SensorEventListener {
    private SensorManager mSensorManager;
    private TextView mProxMain;
    private TextView mProxFar;
    private TextView mProxNear;
    private TextView mProxStatus;
    private boolean mIsFar = false;
    private boolean mIsNear = false;
    private boolean forceSetted = false;
    private int degreeRotation = 0;
    private int degreeRotation_90 = 0;
    private int rotating_animation = 0;
    private WindowManager mWindowManager = null;
    private ContentResolver mContentResolver = null;

    private Handler objHandler = new Handler();

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_proximity, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        mProxMain = (TextView) findViewById(R.id.prox_main);
        mProxFar = (TextView) findViewById(R.id.prox_far);
        mProxNear = (TextView) findViewById(R.id.prox_near);
        mProxStatus = (TextView) findViewById(R.id.prox_status);
        mProxMain.setText(R.string.prox_main_virtual);

        mWindowManager = (WindowManager) mContext.getSystemService(Service.WINDOW_SERVICE);
        mContentResolver = mContext.getContentResolver();
        try {
            degreeRotation = Settings.System.getInt(mContentResolver, "degree_rotation");
            degreeRotation_90 = Settings.System.getInt(mContentResolver, Settings.System.ACCELEROMETER_ROTATION);
        } catch (SettingNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            rotating_animation = Settings.System.getInt(mContentResolver, "rotating_animation");
        } catch (SettingNotFoundException e) {
            // TODO Auto-generated catch block
            Settings.System.putInt(mContentResolver, "rotating_animation", 2);
            rotating_animation = 2;
            e.printStackTrace();
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY),
                SensorManager.SENSOR_DELAY_NORMAL);

        if (degreeRotation == 1) {
            Settings.System.putInt(mContentResolver, "degree_rotation", 0);
        }

        if (degreeRotation_90 == 0) {
            Settings.System.putInt(mContentResolver, Settings.System.ACCELEROMETER_ROTATION, 1);
        }

        if (rotating_animation != 0) {
            Settings.System.putInt(mContentResolver, "rotating_animation", 0);
        }

        int rotation = mWindowManager.getDefaultDisplay().getRotation();
        if (rotation != Surface.ROTATION_180) {
            MMILog.i(TAG, "onResume, force set proximity sensor");
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
            forceSetted = true;
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
        }

        if (forceSetted) {
            MMILog.i(TAG, "onPause, reset proximity sensor");
            mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            forceSetted = false;
        }

        objHandler.postDelayed(run, 300);
    }

    private Runnable run = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub

            if (degreeRotation == 1) {
                Settings.System.putInt(mContext.getContentResolver(), "degree_rotation", 1);
            }

            if (degreeRotation_90 == 0) {
                Settings.System.putInt(mContentResolver, Settings.System.ACCELEROMETER_ROTATION, 0);
            }

            if (rotating_animation != 0) {
                Settings.System.putInt(mContentResolver, "rotating_animation", rotating_animation);
            }
        }
    };

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onSensorChanged(SensorEvent event) {
        int type = event.sensor.getType();

        if (type == Sensor.TYPE_PROXIMITY) {
            float[] value = event.values;

            float result = value[SensorManager.DATA_X];

            if (result == 0) {
                mIsNear = true;
                mProxStatus.setTextColor(Color.WHITE);
                mProxStatus.setText("Near");
                mProxMain.setTextSize(20);
                mProxMain.setTextColor(Color.GRAY);
            } else if (result >= 1.0f) {
                mIsFar = true;
                mProxStatus.setTextColor(Color.WHITE);
                mProxStatus.setText("Far");
                mProxMain.setTextSize(20);
                mProxMain.setTextColor(Color.WHITE);
            }

            if (mIsNear == true) {
                mProxNear.setText("near : OK");
            } else {
                mProxNear.setText("near : not tested");
            }

            if ((mIsNear == true) && (mIsFar == true)) {
                mProxFar.setText("far : OK");
            } else {
                mProxFar.setText("far : not tested");
            }
        }
        if (mIsNear == true && mIsFar == true) {
            setPassButtonEnable(true);
        }
    }
}
