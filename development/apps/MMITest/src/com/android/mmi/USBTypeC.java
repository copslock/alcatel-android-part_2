/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/USBTypeC.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 03/30/16| Shishun.Liu    |                    | FR-694468                  */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.nio.charset.Charset;

import com.android.mmi.util.ExternStorageTest;
import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.VolumeInfo;

public class USBTypeC extends TestBase {
    private String mUSBPlugStatusFile = "/sys/devices/soc/7575000.i2c/i2c-1/1-0050/detect";
    private Handler mHandler = new Handler();
    private boolean mCheckTypeCInfo = false;
    private boolean mCheckPlug1 = false;
    private boolean mCheckPlug2 = false;
    private boolean mCheckPlug1_RW = false;
    private boolean mCheckPlug2_RW = false;
    private String mPlug1TestStatus = "";
    private String mPlug2TestStatus = "";
    private StorageManager storageManager;
    private StorageEventListener storageEventListener;
    private Thread mStorageTestThread = null;
    private String mStoragePath = "";
    private String mTypeCInfo;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        setDefTextMessage("please connect USB disk");
        storageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);
        storageEventListener = new MemberStorageEventListener();
        storageManager.registerListener(storageEventListener);
    }

    @Override
    public void destroy() {
        if(mStorageTestThread!=null && mStorageTestThread.isAlive()) {
            mStorageTestThread.interrupt();
        }
        storageManager.unregisterListener(storageEventListener);
        mHandler.removeCallbacksAndMessages(null);
        super.destroy();
    }

    private Runnable mStorageTestRunnable = new Runnable() {
        @Override
        public void run() {
            if(mCheckTypeCInfo==false) {
                mTypeCInfo = ReadFileString(SysClassManager.FILE_USBTYPE_C);
                if(mTypeCInfo.equalsIgnoreCase("7418")) {
                    mCheckTypeCInfo = true;
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        setDefTextMessage("Type-C ID: " + mTypeCInfo);
                    }
                });
            }
            if(mCheckPlug1==false || mCheckPlug2==false) {
                boolean result = new ExternStorageTest(mStoragePath).startTest();
                String plugStatus = ReadFileString(mUSBPlugStatusFile);
                if (plugStatus.compareToIgnoreCase("f1") == 0) {
                    mCheckPlug1 = true;
                    mCheckPlug1_RW = result;
                    mPlug1TestStatus = (mCheckPlug1_RW ? "OK" : "NOK");
                } else if (plugStatus.compareToIgnoreCase("f4") == 0) {
                    mCheckPlug2 = true;
                    mCheckPlug2_RW = result;
                    mPlug2TestStatus = (mCheckPlug2_RW ? "OK" : "NOK");
                }

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        String status;
                        status = "\nPlug Status1: " + (mCheckPlug1 ? "OK" : "");
                        status += "\nPlug Status1 RW: " + mPlug1TestStatus;
                        status += "\nPlug Status2: " + (mCheckPlug2 ? "OK" : "");
                        status += "\nPlug Status2 RW: " + mPlug2TestStatus;
                        setDefActionMessage(status);
                        setPassButtonEnable(mCheckPlug1 && mCheckPlug1_RW
                                && mCheckPlug2 && mCheckPlug2_RW && mCheckTypeCInfo);
                    }
                });
            }
        }
    };

    private String ReadFileString(String path) {
        SysClassManager fileManager = new SysClassManager();
        byte result[];
        String resultStri;

        fileManager.setFileInputStream(fileManager.fileInputOpen(path));

        result=fileManager.readFileInputByte();
        if(result!=null) {
            fileManager.fileInputClose();
            MMILog.d(TAG, "read len=" + result.length);
            resultStri = new String(result, Charset.forName("US-ASCII"));
            resultStri = resultStri.trim();
            MMILog.d(TAG, "read string = " + resultStri);
        }
        else {
            resultStri = "ERROR";
            MMILog.e(TAG, "Failed to read: "+path);
        }

        return resultStri;
    }

    private class MemberStorageEventListener extends StorageEventListener {
        @Override
        public void onStorageStateChanged(String path, String oldState,
                                          String newState) {
            super.onStorageStateChanged(path, oldState, newState);
            MMILog.i(TAG, "path("+newState+") = " + path);
            if(isUsbStorage(path)) {
                if (Environment.MEDIA_MOUNTED.equals(newState)) {
                    mStoragePath = path;
                    // start storage test
                    try {
                        if(mStorageTestThread!=null && mStorageTestThread.isAlive()) {
                            MMILog.i(TAG, "StorageTestThread is still running, return");
                            //mStorageTestThread.interrupt();
                            return;
                        }
                        mStorageTestThread = new Thread(mStorageTestRunnable);
                        mStorageTestThread.start();
                    }
                    catch(IllegalThreadStateException e) {
                        MMILog.e(TAG, e.toString());
                    }
                } else {
                    mStoragePath = "";
                }
            }
        }

        public void onVolumeStateChanged(VolumeInfo vol, int oldState, int newState) {
            MMILog.d(TAG, vol.toString());
            switch(newState) {
            case VolumeInfo.STATE_MOUNTED:
                mStoragePath = vol.getPath().getAbsolutePath();
                // start storage test
                try {
                    if(mStorageTestThread!=null && mStorageTestThread.isAlive()) {
                        MMILog.i(TAG, "StorageTestThread is still running, return");
                        //mStorageTestThread.interrupt();
                        return;
                    }
                    mStorageTestThread = new Thread(mStorageTestRunnable);
                    mStorageTestThread.start();
                }
                catch(IllegalThreadStateException e) {
                    MMILog.e(TAG, e.toString());
                }
                break;
            //case VolumeInfo.STATE_UNMOUNTED:
            default:
                mStoragePath = "";
                break;
            }
        }
    }

    private boolean isUsbStorage(String path) {
        return !path.contains("emulated") && !path.contains("self");
    }
}