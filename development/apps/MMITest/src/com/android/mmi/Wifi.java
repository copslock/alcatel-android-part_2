/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Vifi.java          */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Wifi extends TestBase {
    public static final int MESSAGE_WIFI_DEFAULT = 1024;
    public static final int MESSAGE_WIFI_DETECT_COMPLETED = MESSAGE_WIFI_DEFAULT + 1;
    public static final int MESSAGE_WIFI_RESTART_REQUEST = MESSAGE_WIFI_DETECT_COMPLETED + 1;
    public static final int LENGTH_WIFI_DETECT_TIMEOUT = 100;
    public static final int LENGTH_WIFI_CHECK_TIME = 1000;

    public static enum WifiMessage {
        WIFI_INIT_WAIT, WIFI_INIT_FAIL, WIFI_DETECT_WAIT, WIFI_DETECT_FAIL, WIFI_DETECT_SUCCESS;
    }

    private boolean isOldWifiEnable;
    private ArrayList<String> wifiScanList;
    private ArrayList<String> wifiScanListWithName;
    private ArrayAdapter<String> wifiScanListAdapter;

    private WifiManager wifiManager;
    private WifiScanReceiver wifiScanReceiver;
    private WifiStatusReceiver wifiStatusReceiver;
    private IntentFilter wifiScanIntentFilter;
    private IntentFilter wifiStatusIntentFilter;
    private Handler handler;

    private TextView messageTextView;
    private Button retryButton;
    private ListView wifiListView;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_wifi, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        wifiScanList = new ArrayList<String>();
        wifiScanListWithName = new ArrayList<String>();
        wifiScanListAdapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_list_item_1, wifiScanList);

        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        wifiScanReceiver = new WifiScanReceiver();
        wifiStatusReceiver = new WifiStatusReceiver();
        wifiScanIntentFilter = new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        wifiStatusIntentFilter = new IntentFilter(
                WifiManager.WIFI_STATE_CHANGED_ACTION);
        handler = new MemberHandler();

        wifiListView = (ListView) findViewById(R.id.wifi_main_list);
        wifiListView.setAdapter(wifiScanListAdapter);
        messageTextView = (TextView) findViewById(R.id.wifi_message);
        retryButton = (Button) findViewById(R.id.wifi_btn_retry);

        initWifi();
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        registerReceivers();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        unregisterReceivers();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        removeAllMessages();
        enableAllAps();
        loadWifiOldState();
    }

    private void initWifi() {
        registerReceivers();
        saveWifiOldState();
        requestWifiRestart();
    }

    private void requestWifiRestart() {
        wifiManager.setWifiEnabled(false);
        sendWifiRestartMessage();
    }

    private void setWifiStartScan() {
        wifiScanList.clear();
        wifiScanListWithName.clear();
        wifiListView.setAdapter(wifiScanListAdapter);
        wifiManager.setWifiEnabled(true);
    }

    private void setWifiScanList() {
        List<ScanResult> wifiRawScanResultList = wifiManager.getScanResults();

        if (wifiRawScanResultList != null) {
            for (ScanResult scanResult : wifiRawScanResultList) {
                String strName = scanResult.SSID.toString();
                if (!wifiScanListWithName.contains(strName)) {
                    wifiScanList.add(strName + "," + scanResult.level);
                    wifiScanListWithName.add(strName);
                }
            }
            wifiListView.setAdapter(wifiScanListAdapter);
        }

        sendWifiDetectCompletedMessage();
    }

    private void setViewStateForWifiScanResult() {
        if (wifiScanList.size() > 0) {
            setViewState(WifiMessage.WIFI_DETECT_SUCCESS);
        } else {
            setViewState(WifiMessage.WIFI_DETECT_FAIL);
        }
    }

    private void setViewState(WifiMessage wifiMessage) {
        switch (wifiMessage) {
        case WIFI_INIT_WAIT:
            messageTextView.setText(R.string.wifi_init_wait);
            retryButton.setEnabled(false);
            setPassButtonEnable(false);
            setFailButtonEnable(false);
            break;
        case WIFI_INIT_FAIL:
            messageTextView.setText(R.string.wifi_init_fail);
            retryButton.setEnabled(true);
            setPassButtonEnable(false);
            setFailButtonEnable(true);
            break;
        case WIFI_DETECT_WAIT:
            messageTextView.setText(R.string.wifi_detect_wait);
            retryButton.setEnabled(false);
            // setPassButtonEnable(true);
            setFailButtonEnable(true);
            break;
        case WIFI_DETECT_FAIL:
            messageTextView.setText(R.string.wifi_detect_fail);
            retryButton.setEnabled(true);
            setPassButtonEnable(false);
            setFailButtonEnable(true);
            break;
        case WIFI_DETECT_SUCCESS:
            messageTextView.setText(R.string.wifi_detect_success);
            retryButton.setEnabled(true);
            setPassButtonEnable(true);
            setFailButtonEnable(true);
            break;
        }
    }

    private boolean isWifiDisabled() {
        if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLED) {
            return true;
        } else {
            return false;
        }
    }

    private void sendWifiRestartMessage() {
        if (isWifiDisabled()) {
            setWifiStartScan();
        } else {
            handler.sendEmptyMessageDelayed(MESSAGE_WIFI_RESTART_REQUEST,
                    LENGTH_WIFI_CHECK_TIME);
        }
    }

    private void sendWifiDetectCompletedMessage() {
        handler.removeMessages(MESSAGE_WIFI_DETECT_COMPLETED);
        handler.sendEmptyMessageDelayed(MESSAGE_WIFI_DETECT_COMPLETED,
                LENGTH_WIFI_DETECT_TIMEOUT);
    }

    private void removeAllMessages() {
        handler.removeMessages(MESSAGE_WIFI_RESTART_REQUEST);
        handler.removeMessages(MESSAGE_WIFI_DETECT_COMPLETED);
    }

    private void saveWifiOldState() {
        isOldWifiEnable = wifiManager.isWifiEnabled();
    }

    private void loadWifiOldState() {
        wifiManager.setWifiEnabled(isOldWifiEnable);
    }

    private void registerReceivers() {
        mContext.registerReceiver(wifiStatusReceiver, wifiStatusIntentFilter);
        mContext.registerReceiver(wifiScanReceiver, wifiScanIntentFilter);
    }

    private void unregisterReceivers() {
        mContext.unregisterReceiver(wifiScanReceiver);
        mContext.unregisterReceiver(wifiStatusReceiver);
    }

    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_WIFI_DETECT_COMPLETED:
                setViewStateForWifiScanResult();
                break;
            case MESSAGE_WIFI_RESTART_REQUEST:
                sendWifiRestartMessage();
                break;
            }
        }
    }

    // TODO combine receivers, get one receiver
    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            setWifiScanList();
        }
    }

    private class WifiStatusReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            final int wifiState = intent.getIntExtra(
                    WifiManager.EXTRA_WIFI_STATE,
                    WifiManager.WIFI_STATE_UNKNOWN);

            switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLING:
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_ENABLING:
                setViewState(WifiMessage.WIFI_INIT_WAIT);
                break;
            case WifiManager.WIFI_STATE_ENABLED:
                setViewState(WifiMessage.WIFI_DETECT_WAIT);
                disableAllAps();
                wifiManager.disconnect();
                wifiManager.startScan(); //[BUGFIX]-ADD by TCTNB.Ji.Chen,2015/1/14,PR903223,fix wifi network list display delay 15 sec issue
                break;
            case WifiManager.WIFI_STATE_UNKNOWN:
                setViewState(WifiMessage.WIFI_INIT_FAIL);
                break;
            }
        }
    }
  //[BUGFIX] START, take form m823 pr#1011441
    private void disableAllAps() {
        List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
        if (null != configs) {
            for (WifiConfiguration config : configs) {
                if (config.networkId != -1 &&
                     config.status == WifiConfiguration.Status.ENABLED) {
                    wifiManager.disableNetwork(config.networkId);
                }
            }
        }
    }

    private void enableAllAps() {
        List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
        if (null != configs) {
            for (WifiConfiguration config : configs) {
                if ( config.networkId != -1 &&
                    config.status == WifiConfiguration.Status.DISABLED) {
                    wifiManager.enableNetwork(config.networkId,false);
                }
            }
        }
    }
    //[BUGFIX] END
}