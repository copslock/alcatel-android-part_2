/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/util/              */
/*                                                           DisplayView.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

// NOTICE #Fixing_DisplayView
public class DisplayView extends View {
    public static enum DisplayItem {
        COLOR, MONO, BLACK, WHITE, GRAY
    };

    private int viewWidth;
    private int viewHeight;
    private DisplayItem displayItem;

    private Paint paint;

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Constructors
    // //////////////////////////////////////////////////////////////////////////////
    public DisplayView(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
        initResource();
    }

    public DisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initResource();
    }

    public DisplayView(Context context) {
        super(context);
        initResource();
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Initialize Methods
    // //////////////////////////////////////////////////////////////////////////////
    private void initResource() {
        paint = new Paint();
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Override Methods
    // //////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawPattern(canvas);
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Methods
    // //////////////////////////////////////////////////////////////////////////////
    /* *****************************************************************************
     * METHOD select draw display item
     * ******************************************
     * *********************************
     */
    private void drawPattern(Canvas c) {
        paint.reset();
        switch (displayItem) {
        case COLOR:
            drawCoulourPattern(c);
            break;
        case BLACK:
            drawBlackPattern(c);
            break;
        case MONO:
            drawGrayChartPattern(c);
            break;
        case WHITE:
            drawWhitePattern(c);
            break;
        case GRAY:
            drawGrayPattern(c);
        }
    }

    // TODO request code batterment
    /* *****************************************************************************
     * METHOD draw 'color' display item
     * *****************************************
     * **********************************
     */
    private void drawCoulourPattern(Canvas c) {
        Rect rect = new Rect(0, 0, viewWidth, viewHeight / 3);
        paint.setColor(Color.RED);
        c.drawRect(rect, paint);

        rect.set(0, viewHeight / 3, viewWidth, viewHeight / 3 * 2);
        paint.setColor(Color.GREEN);
        c.drawRect(rect, paint);

        rect.set(0, viewHeight / 3 * 2, viewWidth, viewHeight);
        paint.setColor(Color.BLUE);
        c.drawRect(rect, paint);

        paint.setColor(Color.BLACK);
        paint.setTextSize(50);
        paint.setAntiAlias(true);
        // c.drawText("LCD", viewWidth / 3 + 5, viewHeight / 2 + 10, paint);
    }

    private void drawBlackPattern(Canvas c) {
        Rect rect = new Rect(0, 0, viewWidth, viewHeight);
        paint.setColor(Color.BLACK);
        c.drawRect(rect, paint);
    }

    private void drawGrayChartPattern(Canvas c) {
        int r = 0;
        int g = 0;
        int b = 0;
        final int step = 20;

        int rectR = viewHeight / step;
        Rect rect = new Rect(0, 0, viewWidth, rectR);

        for (int i = 0; i < step; i++) {
            paint.setColor(Color.argb(255, r, g, b));
            c.drawRect(rect, paint);

            r = g = b = r + (255 / step);
            rect.set(0, rect.bottom, viewWidth, rect.bottom + rectR);
            if (r >= 255) {
                break;
            }
        }
    }

    private void drawWhitePattern(Canvas c) {
        Rect rect = new Rect(0, 0, viewWidth, viewHeight);
        paint.setColor(Color.WHITE);
        c.drawRect(rect, paint);
    }

    private void drawGrayPattern(Canvas c) {
        Rect rect = new Rect(0, 0, viewWidth, viewHeight);
        paint.setColor(Color.GRAY);
        c.drawRect(rect, paint);
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Getter/Setters
    // //////////////////////////////////////////////////////////////////////////////
    public void setViewSize(int viewWidth, int viewHeight) {
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
    }

    public void setDisplayItem(DisplayItem displayItem) {
        this.displayItem = displayItem;
        invalidate();
    }
}
