/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/ChargeLed.java     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.os.SystemProperties;
import android.provider.Settings;

public class ChargeLed extends TestBase {
    private final static int CHARGE_LED_ID = 0;
    private NotificationManager mNotificationManager = null;
    private ContentResolver mContentResolver = null;
    private boolean tctMMITest = false;
    private int notificationLight = 0;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mNotificationManager = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
        mContentResolver = mContext.getContentResolver();
        tctMMITest = SystemProperties.getBoolean("dev.tct.MMITest", false);
        notificationLight = Settings.System.getInt(mContentResolver, Settings.System.NOTIFICATION_LIGHT_PULSE, 0);

        mContext.mTextMessage.setText(R.string.chargeled_red);
        setPassButtonEnable(true);
        setButtonAnimateVisible();
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        if (!tctMMITest) {
            SystemProperties.set("dev.tct.MMITest", "true");
        }

        if (notificationLight == 0) {
            Settings.System.putInt(mContentResolver, Settings.System.NOTIFICATION_LIGHT_PULSE, 1);
        }

        Notification.Builder mBuild = new Notification.Builder(mContext);
        mBuild.setSmallIcon(R.drawable.noti_icon);
        mBuild.setDefaults(Notification.DEFAULT_VIBRATE);
        Notification notice = mBuild.build();

        notice.ledARGB = 0xffffffff;
        notice.flags |= Notification.FLAG_SHOW_LIGHTS;
        notice.ledOnMS = 1;
        notice.ledOffMS = 0;
        mNotificationManager.notify(CHARGE_LED_ID, notice);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mNotificationManager.cancel(CHARGE_LED_ID);
        if (!tctMMITest) {
            SystemProperties.set("dev.tct.MMITest", "false");
        }

        if (notificationLight == 0) {
            Settings.System.putInt(mContentResolver, Settings.System.NOTIFICATION_LIGHT_PULSE, 0);
        }
    }
}
