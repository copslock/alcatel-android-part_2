/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/AutoTestMain.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.Value;
import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.TestItemManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import com.android.mmi.util.MMILog;

public class AutoTestMain extends TestBase {
    private SharedPreferences mPreference;
    private SharedPreferences.Editor mEditor;
    private final int TEST_NOT_START = -1;
    private final int TEST_ALL_PASS = -2;
    private int currentItemIdx;
    private int mLastFailIdx;

    private TraceabilityStruct traceabilityStruct;
    private byte mINFO_STATUS_MMI_TEST = (byte) 0xFF;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mPreference = mContext.getSharedPreferences("pref", Activity.MODE_PRIVATE);
        mEditor = mPreference.edit();
        reset();
        setPassButtonEnable(true);
        try{
            traceabilityStruct = new TraceabilityStruct();
        }catch (Exception e){
            e.printStackTrace();
        }

        getMMITestStatusFromNv();
    }

    private void reset() {
        mLastFailIdx = mPreference.getInt(Value.USER_SELECT, TEST_NOT_START);
        if(mLastFailIdx==TEST_NOT_START){
            mContext.mTextMessage.setText(R.string.auto_no_test);
            mContext.mTextMessage.setTextColor(Color.BLUE);
            mContext.mTextAction.setText(R.string.auto_start);
            mContext.mPassButton.setText(R.string.auto_button_yes);
            mContext.mFailButton.setText(R.string.auto_button_no);
        }
        else if(mLastFailIdx==TEST_ALL_PASS){
            mContext.mTextMessage.setText(R.string.auto_all_pass);
            mContext.mTextMessage.setTextColor(Color.GREEN);
            mContext.mTextAction.setText("");
            mContext.mPassButton.setText(R.string.auto_button_restart);
            mContext.mFailButton.setText(R.string.auto_button_end);
            // reset it to TEST_NOT_START for restart next time
            mEditor.putInt(Value.USER_SELECT, TEST_NOT_START);
            mEditor.commit();
        }
        else{
            mContext.mTextMessage.setText(String.format("Failed: %03d\n", mLastFailIdx)
                    + TestItemManager.GetTestItemInfoString(mLastFailIdx, "title"));
            mContext.mTextMessage.setTextColor(Color.RED);
            mContext.mTextAction.setText(R.string.auto_restart);
            mContext.mPassButton.setText(R.string.auto_button_yes);
            mContext.mFailButton.setText(R.string.auto_button_no);
        }
        mLastFailIdx = TEST_NOT_START;
        currentItemIdx = 0;
    }

    @Override
    public void onPassClick() {
        while( currentItemIdx<TestItemManager.getTestItemCount() &&
                (TestItemManager.GetTestItemInfoInt(currentItemIdx, "deftype")&
                TestItemManager.DEF_TYPE_AUTO1)!= TestItemManager.DEF_TYPE_AUTO1 ){
            currentItemIdx++;
        }
        if(currentItemIdx<TestItemManager.getTestItemCount()) {
            Intent intent;
            TestItemManager.setCurrentTestItem(currentItemIdx);
            intent = new Intent(mContext, CommonActivity.class);
            intent.putExtra(Value.USER_SELECT, Value.USER_SELECT_MODE_AUTO);
            mContext.startActivityForResult(intent, Value.SCENARIO_AUTO);
        }
        else {
            if(mLastFailIdx<=TEST_NOT_START) {
                MMILog.d(TAG,"set mmi result true");
                setMmiResultToNv(true);
                mLastFailIdx=TEST_ALL_PASS;
                mEditor.putInt(Value.USER_SELECT, mLastFailIdx);
                mEditor.commit();
            }
            reset();
        }
    }

    @Override
    public void onFailClick() {
        if( mContext.getString(R.string.auto_button_end).equals(mContext.mFailButton.getText()) ){
            MiniShutdown();
        }
        else {
            mContext.finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            MMILog.d(TAG, "onActivityResult error: data == null");
            return;
        }
        if (requestCode == Value.SCENARIO_AUTO) {
            if (resultCode == Value.RESULT_CODE_PASS) {
                if(currentItemIdx==mLastFailIdx) {
                    mLastFailIdx = TEST_NOT_START;
                    mEditor.putInt(Value.USER_SELECT, mLastFailIdx);
                    mEditor.commit();
                }
                currentItemIdx++;
                if(currentItemIdx<TestItemManager.getTestItemCount()) {
                    onPassClick();
                }
                else if(mLastFailIdx<=TEST_NOT_START) {
                    mLastFailIdx=TEST_ALL_PASS;
                    MMILog.d(TAG,"set mmi result true");
                    setMmiResultToNv(true);
                    mEditor.putInt(Value.USER_SELECT, mLastFailIdx);
                    mEditor.commit();
                    reset();
                }
                else {
                    reset();
                }
            } else if (resultCode == Value.RESULT_CODE_FAIL) {
                if(mLastFailIdx<=TEST_NOT_START || currentItemIdx<mLastFailIdx) {
                    mLastFailIdx = currentItemIdx;
                    setMmiResultToNv(false);
                    mEditor.putInt(Value.USER_SELECT, mLastFailIdx);
                    mEditor.commit();
                }
                if(currentItemIdx<TestItemManager.getTestItemCount()-1){
                    mContext.mTextMessage.setText(String.format("Failed: %03d\n", currentItemIdx)
                            + TestItemManager.GetTestItemInfoString(currentItemIdx, "title"));
                    mContext.mTextMessage.setTextColor(Color.RED);
                    mContext.mTextAction.setText(R.string.auto_next);
                    mContext.mPassButton.setText(R.string.auto_button_next);
                    mContext.mFailButton.setText(R.string.auto_button_end);
                    currentItemIdx++;
                }
                else{
                    reset();
                }
            } else if (resultCode == Value.RESULT_CODE_RESTART) {
                if(mLastFailIdx<=TEST_NOT_START || currentItemIdx<mLastFailIdx) {
                    mLastFailIdx = currentItemIdx;
                    mEditor.putInt(Value.USER_SELECT, mLastFailIdx);
                    mEditor.commit();
                }
                setDefTextMessage(R.string.test_interrupted, Color.WHITE);
                setDefActionMessage("");
                setPassButtonText(R.string.common_button_auto_restart);
                setFailButtonText(R.string.auto_button_no);
            }
        }
    }

    private void setMmiResultToNv(boolean flag) {
        if (!flag) {
            if(mINFO_STATUS_MMI_TEST == 1 || mINFO_STATUS_MMI_TEST == 3){// auto1 is pass before
                 mINFO_STATUS_MMI_TEST -= (byte) 0x01;
            }
        }else{
            if(mINFO_STATUS_MMI_TEST != 1 && mINFO_STATUS_MMI_TEST != 3){// auto1 not pass before
                mINFO_STATUS_MMI_TEST += (byte) 0x01;
            }
        }

        byte[] result = {mINFO_STATUS_MMI_TEST};

        if (!BuildConfig.isSW && traceabilityStruct != null) {
            traceabilityStruct.putItem(
                    TraceabilityStruct.ID.INFO_STATUS_MMI_TEST_I, result);
        }
    }

    private void getMMITestStatusFromNv() {
        byte[] data = {0};

        if (!BuildConfig.isSW && traceabilityStruct != null) {
            data = traceabilityStruct.getItem(TraceabilityStruct.ID.INFO_STATUS_MMI_TEST_I);
            mINFO_STATUS_MMI_TEST = data[0];
        }
    }

    @Override
    public void onBackPressed() {
        mContext.finish();
    }

    private void MiniShutdown() {
        Intent shutdown = new Intent(Intent.ACTION_REQUEST_SHUTDOWN);
        shutdown.putExtra(Intent.EXTRA_KEY_CONFIRM, false);
        shutdown.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(shutdown);
    }
}
