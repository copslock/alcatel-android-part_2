/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/MainScreen.java    */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import static com.waves.maxxutil.MaxxDefines.MSG_ASYNC;
import com.waves.maxxclientbase.MaxxHandle.EnableStateChangeListener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioSystem;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.SystemProperties;
import com.android.mmi.util.MMILog;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import com.waves.maxxclientbase.MaxxHandle;
import com.waves.maxxutil.MaxxDefines;

import com.android.mmi.BuildConfig;
import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.JRDClient;
import com.android.mmi.util.KeyCodeFilter;
import com.android.mmi.util.TestItemManager;
import com.android.mmi.util.VersionAPI;


public class MainScreen extends TestBase {
    private String mModemVersion = "????????";
    private String mPCBA_I = null;
    private String mBSN = null;
    private boolean mRequestReboot = false;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mContext.mTextTitle.setTextColor(Color.RED);
        mContext.mPassButton.setText(R.string.common_button_main_left);
        mContext.mFailButton.setText(R.string.common_button_main_right);
        setFailButtonEnable(false);

        dismissKeyGuard();

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {
                if(TestItemManager.getProduct().equals("idol4s")) {
                    getSpeakerStatus();
                }
                getVersion();
                get_SN_PCBA();
                return getBuildNumber();
            }

            @Override
            protected void onPostExecute(String result) {
                mContext.mTextMessage.setText(result);
                TestItemManager.setBSN(mBSN);
                if(mRequestReboot) {
                    AlertDialog al = new AlertDialog.Builder((Context)mContext)
                            .setTitle(R.string.reboottitle)
                            .setMessage(R.string.reboot)
                            .setCancelable(false)
                            .show();

                    Handler handler = new Handler();
                    handler.postDelayed(rebootRunnable, 5000);
                }
                else {
                    setAuto2ButtonEnable(true);
                    setPassButtonEnable(true);
                    setFailButtonEnable(true);
                }
            }
        }.execute();

        Intent intent = mContext.getIntent();
        if(intent != null) {
            boolean isRobust = intent.getBooleanExtra("robust_tp_raw", false);
            MMILog.d(TAG, "isRobust: " + isRobust);
            if(isRobust){
                HashMap<String, Object> itemHash = TestItemManager.GetTestItemInfoFromTitle("Raw Data");
                if(itemHash!=null) {
                    String title = (String)itemHash.get("title");
                    String className = (String)itemHash.get("defclass");

                    intent.setClass(mContext, CommonActivity.class);
                    TestItemManager.setCurrentItem(className, title);
                    mContext.startActivityForResult(intent, Value.REQ_ROBUST_TP_RAW);
                    return;
                }
            }
        }

        disableNfc();

        setMaxxAudioOff();
    }

    private void disableNfc(){
        NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        if (mNfcAdapter != null) {
            if(mNfcAdapter.isEnabled()){
                mNfcAdapter.disable();
                MMILog.d(TAG, "disable NFC Adatper");
            }
        }
    }

    private void setMaxxAudioOff(){
        new Thread(new Runnable() {
            public void run() {
                final MaxxHandle mMaxxHandle = new MaxxHandle(mContext);
                mMaxxHandle.SetAlgType(MaxxDefines.ALG_TYPE_MAXXMOBILE2);
                mMaxxHandle.SetServiceName("com.waves.maxxservice.MaxxService");
                if (!mMaxxHandle.connectService()) {
                    MMILog.e(TAG, "mMaxxHandle connectService failed");
                } else {
                    MMILog.i(TAG, "mMaxxHandle connectService successful");
                }
                mMaxxHandle.SetServiceConnectListener(new MaxxHandle.ServiceConnectListener() {
                    @Override
                    public void OnServiceConnected() {
                        /* MODIFIED-BEGIN by zhengyang.ma, 2016-03-22, BUG-1810982 */
                        MMILog.d(TAG, "[OnServiceConnected]");

                        mMaxxHandle.GetMaxxSenseEnabled(MSG_ASYNC);
                        mMaxxHandle.GetNotificationBarEnabled(MSG_ASYNC);
                        mMaxxHandle.SetEnabled(MSG_ASYNC, false);
                        /* MODIFIED-END by zhengyang.ma,BUG-1810982 */

                        mMaxxHandle.disconnectService();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        // stop finish here
    }

    @Override
    public void onPassClick() {
        StartTestMode("AutoTestMain");
    }

    @Override
    public void onFailClick() {
        StartTestMode("MiniTestMenu");
    }

    @Override
    public void onAuto2Click() {
        StartTestMode("Auto2TestMain");
    }

    void StartTestMode(String testMode) {
        Value.isAutoTest = testMode.equals("AutoTestMain");
        Value.isAuto2Test = testMode.equals("Auto2TestMain");
        TestItemManager.setCurrentItem(testMode, mContext.getString(R.string.auto_title));
        Intent intent = new Intent(mContext, CommonActivity.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void getVersion() {

        VersionAPI v= new VersionAPI();
        String version = v.getBootVer();/*system version*/

        if(version.length()>=7) {
            mModemVersion = version.substring(1,4) + version.substring(6,7);
        }
    }

    private void get_SN_PCBA() {

        TraceabilityStruct ts = null;
        try {
            ts = new TraceabilityStruct();
        } catch (Exception e) {
            MMILog.d(TAG, e.toString());
        }

        if (ts != null) {
            mBSN = new String(ts.getItem(TraceabilityStruct.ID.SHORT_CODE_I))
                    + new String(ts.getItem(TraceabilityStruct.ID.ICS_I))
                    + new String(ts.getItem(TraceabilityStruct.ID.SITE_FAC_PCBA_I))
                    + new String(ts.getItem(TraceabilityStruct.ID.LINE_FAC_PCBA_I))
                    + new String(ts.getItem(TraceabilityStruct.ID.DATE_PROD_PCBA_I))
                    + new String(ts.getItem(TraceabilityStruct.ID.SN_PCBA_I));

            mPCBA_I = new String(ts.getItem(TraceabilityStruct.ID.SHORT_CODE_I))
                    .substring(0, 3)
                    + "_"
                    + new String(ts.getItem(TraceabilityStruct.ID.SN_PCBA_I));
            if (!BuildConfig.isSW) {
                ts.putItem(TraceabilityStruct.ID.INFO_PTS_MINI_I, (mModemVersion).substring(1,4).getBytes());
            }
        }
    }

    private String getBuildNumber() {
        String mode = "";
        int result = TestItemManager.getPhoneMode();
        if(result == 0){
            mode = "ONLINE";
        } else if(result == 1){
            mode = "FTM";
        } else {
            mode = "unknown";
        }

        String str;
        if(TestItemManager.getProduct().equals("unknown")) {
            str = "MMI TEST\n" + "SW : " + mModemVersion +"\n"+ "SN : " + mPCBA_I;
        }
        else if(TestItemManager.getProduct().equals("idol4s_cn")) {
            str = "IDOL4 PRO CN" + " MMI TEST\n" + "SW : " + mModemVersion + "\n" + "SN : " + mPCBA_I;
        }
        else {
            str = TestItemManager.getProduct().toUpperCase() + " MMI TEST\n" + "SW : " + mModemVersion +"\n"+ "SN : " + mPCBA_I;
        }

        str += ("\n\n" + mode);

        return str;
    }

    private void dismissKeyGuard(){
        mContext.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(event.getAction()==KeyEvent.ACTION_DOWN) {
            if( ProcessKeyEvent(event.getKeyCode()) ) {
                return true;
            }
        }
        return mContext.super_dispatchKeyEvent(event);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // key code is in resultCode if it need to process

        if(requestCode == Value.REQ_ROBUST_TP_RAW) {
            mContext.setResult(resultCode, data);
            mContext.finish();
            return;
        }

        if(resultCode != android.app.Activity.RESULT_CANCELED &&
                resultCode != Value.RESULT_CODE_FAIL &&
                resultCode != Value.RESULT_CODE_PASS &&
                resultCode != Value.RESULT_CODE_ERROR &&
                resultCode != Value.RESULT_CODE_RESTART) {
            ProcessKeyEvent(resultCode);
        }
    }

    private boolean ProcessKeyEvent(int keycode) {
        if(KeyCodeFilter.isUserCustomizableKeycode(keycode)==false) {
            return false;
        }

        HashMap<String, Object> itemHash = null;
        int deftype = 0;

        if(keycode == 16){
            itemHash = TestItemManager.GetTestItemInfoFromTitle("SIM");
        }else if(keycode == 30){
            itemHash = TestItemManager.GetTestItemInfoFromTitle("Memory Card");
        }else{
            MMILog.d(TAG, "only support keyevent 16 or 30");
        }

        if(itemHash!=null) {
            String title = (String)itemHash.get("title");
            String className = (String)itemHash.get("defclass");

            try{
                deftype = Integer.parseInt((String)itemHash.get("deftype"));
            }
            catch(NumberFormatException e){
                MMILog.d(TAG, "Invalid integer value, keyevent="+keycode);
            }

            if(deftype!=0) {
                Intent intent = new Intent();
                intent.setClass(mContext, CommonActivity.class);
                TestItemManager.setCurrentItem(className, title);
                intent.putExtra(Value.USER_SELECT, Value.USER_SELECT_MODE_KEYEVENT);
                mContext.startActivityForResult(intent, 0);

                return true;
            }
            else {
                MMILog.d(TAG, "keycode("+keycode+") - \""+title + "\" is not supported for this product");
            }
        }
        else {
            MMILog.d(TAG, "keycode("+keycode+") not defined, skip");
        }
        return false;
    }

    private void getSpeakerStatus() {
        byte[] flag_AAC = {0x57, 0x68, 0x79, (byte)0x8a};
        byte[] flag_LC = {0x1f, 0x2e, 0x3c, 0x4b};
        byte flag = 'u'; // unknown
        byte gpio = 'u'; // unknown
        TraceabilityStruct traceabilityStruct = null;
        try {
            traceabilityStruct = new TraceabilityStruct();
            byte[] flag_read = traceabilityStruct.getItem(TraceabilityStruct.ID.INFO_SPEAKER);
            if(flag_read!=null && flag_read.length==4) {
                if(Arrays.equals(flag_read, flag_AAC)) {
                    flag = 'a';
                }
                else if(Arrays.equals(flag_read, flag_LC)) {
                    flag = 'l';
                }
            }
        } catch (Exception e) {
            MMILog.e(TAG, e.toString());
        }
        try {
            BufferedReader bufReader = new BufferedReader(new FileReader("/sys/class/tfa9890/control/spk_box_type"));
            String info = bufReader.readLine();
            MMILog.e(TAG, "Speaker GPIO: "+info);
            if(info!=null) {
                if(info.equals("AAC_IDOL4S")) {
                    gpio = 'a';
                }
                else if(info.equals("LC_IDOL4S")) {
                    gpio = 'l';
                }
            }
        }
        catch(FileNotFoundException e) {
            MMILog.e(TAG, e.toString());
        }
        catch(IOException e) {
            MMILog.e(TAG, e.toString());
        }

        // write flag
        if( gpio!='u' && flag!=gpio && traceabilityStruct!=null ) {
            traceabilityStruct.putItem(TraceabilityStruct.ID.INFO_SPEAKER,
                    gpio=='a'?flag_AAC:flag_LC);
            mRequestReboot = true;
        }
    }

    private Runnable rebootRunnable = new Runnable() {
        @Override
        public void run() {
            Intent reboot = new Intent(Intent.ACTION_REBOOT);
            reboot.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(reboot);
        }
    };
}
