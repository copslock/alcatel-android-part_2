/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                            KeyBLTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.view.KeyEvent;

class KeyBLTest extends Test {
    String TAG = Test.TAG + "KeyBLTest";
    private TestLayout1 tl;

    KeyBLTest(ID pid, String s) {
        super(pid, s);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    if (event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
                        // tl.setAutoTestButtons(true);
                        return true;
                    }
                }

                return false;
            }
        };
        //
    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT:
            // init the test, shows the first screen
            tl = new TestLayout1(mContext, mName, "is key-backlight on?");

            // tl.setAutoTestButtons(false);
            // tl.setAutoTestButtons(true);
            mContext.setContentView(tl.ll);
            break;

        default:
            break;
        }
    }

}
