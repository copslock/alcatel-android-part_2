/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                  TouchWindowLineTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Context;
import android.graphics.*;
import android.util.TctLog;
import android.view.MotionEvent;
import android.view.View;
import android.view.KeyEvent;

class TouchWindowLineTest extends Test {

    TestLayout1 tl;

    private String TAG = "TouchWindowLineTest";

    public final static int VERTICAL_TEST = INIT;
    public final static int VERTICAL_TEST_PASS = INIT + 1;

    private int mGoodLinesCount;
    private int mWidth = 50;
    private float mAverageX = 0;
    private float mAverageY = 0;

    private Point[] hRect1 = { new Point(0, 0), new Point(Lcd.width(), 0),
            new Point(Lcd.width(), mWidth), new Point(0, mWidth), };

    private Point[] hRect2 = {
            new Point(0, (Lcd.height() - 3 * mWidth) / 4 * 2 + mWidth),
            new Point(Lcd.width(), (Lcd.height() - 3 * mWidth) / 4 * 2 + mWidth),
            new Point(Lcd.width(), (Lcd.height() - 3 * mWidth) / 4 * 2 + mWidth
                    * 2),
            new Point(0, (Lcd.height() - 3 * mWidth) / 4 * 2 + mWidth * 2), };

    private Point[] hRect3 = { new Point(0, Lcd.height() - mWidth),
            new Point(Lcd.width(), Lcd.height() - mWidth),
            new Point(Lcd.width(), Lcd.height()), new Point(0, Lcd.height()), };

    private Point[] vRect1 = { new Point(0, 0), new Point(0, Lcd.height()),
            new Point(mWidth, Lcd.height()), new Point(mWidth, 0), };

    private Point[] vRect2 = {
            new Point((Lcd.width() - 3 * mWidth) / 4 * 2 + mWidth, 0),
            new Point((Lcd.width() - 3 * mWidth) / 4 * 2 + mWidth, Lcd.height()),
            new Point((Lcd.width() - 3 * mWidth) / 4 * 2 + mWidth * 2,
                    Lcd.height()),
            new Point((Lcd.width() - 3 * mWidth) / 4 * 2 + mWidth * 2, 0), };

    private Point[] vRect3 = { new Point((Lcd.width() - mWidth), 0),
            new Point((Lcd.width() - mWidth), Lcd.height()),
            new Point(Lcd.width(), Lcd.height()), new Point(Lcd.width(), 0), };

    TouchWindowLineTest(ID pid, String s) {
        super(pid, s);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                return true;
            }
        };
    }

    TouchWindowLineTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                return true;
            }
        };
    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case VERTICAL_TEST:
            // result will be set to false if the pen goes out of the shapes
            Result = NOT_TESTED;
            mContext.setContentView(new MyView(mContext, 1));
            mGoodLinesCount = 0;
            break;
        case VERTICAL_TEST_PASS:
            StopTimer();
            Result = Test.PASSED;
            Exit();
        default:
            break;
        }
    }

    public class MyView extends View {

        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;
        private Paint mPaint;
        private AlertDialog mAlertDialog, mAlertDialogMsg, mAlertDialogEnd,
                mAlertDialogPass;

        private Parallelepipede pl1;
        private Parallelepipede pl2;
        private Parallelepipede pl3;
        private Parallelepipede pl4;
        private Parallelepipede pl5;
        private Parallelepipede pl6;

        private static final int HORIZONTAL = 0;
        private static final int VERTICAL = 1;
        private int mMinLen = 0;
        private int mLineUnderTest = 0;
        private int mLine1Pass = 0;
        private int mLine2Pass = 0;
        private int mLine3Pass = 0;
        private int mLine4Pass = 0;
        private int mLine5Pass = 0;
        private int mLine6Pass = 0;

        public MyView(Context c, int orientation) {
            super(c);
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setColor(0xFFFF0000);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.BEVEL);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(1);
            mBitmap = Bitmap.createBitmap(Lcd.width(), Lcd.height(),
                    Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);

            // if(HORIZONTAL == orientation) {
            pl1 = new Parallelepipede(hRect1);
            pl2 = new Parallelepipede(hRect2);
            pl3 = new Parallelepipede(hRect3);
            // } else if(VERTICAL == orientation) {
            pl4 = new Parallelepipede(vRect1);
            pl5 = new Parallelepipede(vRect2);
            pl6 = new Parallelepipede(vRect3);
            // } else {
            // do nothing
            // }
            mMinLen = Lcd.width() - 40;
            mAlertDialogEnd = new AlertDialog.Builder(mContext)
                    .setTitle("TEST RESULT")
                    .setMessage("Pen out of bounds!")
                    .setNegativeButton("FAIL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    ExecuteTest.currentTest.Result = Test.FAILED;
                                    ExecuteTest.currentTest.Exit();
                                }
                            })
                    .setPositiveButton("RETEST",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    ExecuteTest.currentTest.Run();
                                }
                            }).create();

            mAlertDialogMsg = new AlertDialog.Builder(mContext)
                    .setTitle("TEST RESULT")
                    .setMessage("line too short!")
                    .setNeutralButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    ExecuteTest.currentTest.Run();
                                }
                            }).create();

        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawColor(Color.BLACK);
            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

            /* draw 2 parallelepipede on the screen */
            pl1.draw(canvas);
            pl2.draw(canvas);
            pl3.draw(canvas);
            pl4.draw(canvas);
            pl5.draw(canvas);
            pl6.draw(canvas);

            /* draw references lines on the screen */
            Paint p = new Paint();
            p.setColor(Color.WHITE);
            p.setStyle(Paint.Style.STROKE);
            p.setTextSize(20);
            p.setTextAlign(Paint.Align.CENTER);

            // header text
            canvas.drawText("Please draw in the yellow area", Lcd.width() / 2,
                    40, p);
            /* draw the current pen position */
            canvas.drawPath(mPath, mPaint);

        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 1;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
        }

        private void touch_up() {
            mPath.lineTo(mX, mY);
            // commit the path to our offscreen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            // mPath.reset();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            // check if the point is inside the bounds drawn on the screen
            if (pl1.includePoint(x, y)) {
                mLineUnderTest = 1;
            } else if (pl2.includePoint(x, y)) {
                mLineUnderTest = 2;
            } else if (pl3.includePoint(x, y)) {
                mLineUnderTest = 3;
            } else if (pl4.includePoint(x, y)) {
                mLineUnderTest = 4;
            } else if (pl5.includePoint(x, y)) {
                mLineUnderTest = 5;
            } else if (pl6.includePoint(x, y)) {
                mLineUnderTest = 6;
            } else {
                Result = FAILED;
            }

            TctLog.d(TAG, "x = " + x + " y = " + y);

            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                mAverageX = x;
                mAverageY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                mAverageX = (x + mAverageX) / 2;
                mAverageY = (y + mAverageY) / 2;
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();

                TctLog.d(TAG, "AVERAGES : x = " + mAverageX + " y = " + mAverageY);

                /* check the length of the path */
                RectF rect = new RectF(0, 0, 0, 0);
                mPath.computeBounds(rect, true);
                float mPathLength = (float) Math.sqrt(rect.height()
                        * rect.height() + rect.width() * rect.width());
                TctLog.i(TAG, "path length is " + mPathLength);

                if (Result == FAILED) {
                    mAlertDialog = mAlertDialogEnd;
                } else if (mPathLength < mMinLen) {
                    mAlertDialog = mAlertDialogMsg;
                } else {
                    if (1 == mLineUnderTest) {
                        mLine1Pass = 1;
                    } else if (2 == mLineUnderTest) {
                        mLine2Pass = 1;
                    } else if (3 == mLineUnderTest) {
                        mLine3Pass = 1;
                    } else if (4 == mLineUnderTest) {
                        mLine4Pass = 1;
                    } else if (5 == mLineUnderTest) {
                        mLine5Pass = 1;
                    } else if (6 == mLineUnderTest) {
                        mLine6Pass = 1;
                    }
                    mGoodLinesCount = mLine1Pass + mLine2Pass + mLine3Pass
                            + mLine4Pass + mLine5Pass + mLine6Pass;

                    if (6 == mGoodLinesCount) {
                        SetTimer(500 /* ms */, new CallBack() {
                            public void c() {
                                mState = VERTICAL_TEST_PASS;
                                Run();
                            }
                        });
                    } else {
                        mAlertDialog = null;
                    }
                }

                Result = NOT_TESTED;

                if (mAlertDialog == null) {
                    // do nothing
                } else if (!mAlertDialog.isShowing()) {
                    mAlertDialog.show();
                }

                break;
            }
            return true;
        }
    }

    class Parallelepipede {
        private Path mPath;
        private Paint mPaint;
        private Point[] points;

        Parallelepipede(Point[] p) {
            points = p.clone();
            mPath = new Path();
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setColor(Color.YELLOW);
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setStrokeJoin(Paint.Join.BEVEL);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(1);
        }

        void draw(Canvas c) {
            mPath.reset();
            mPath.moveTo(points[0].x, points[0].y);
            for (int i = 1; i < points.length; i++) {
                mPath.lineTo(points[i].x, points[i].y);
            }
            mPath.close();
            c.drawPath(mPath, mPaint);
        }

        /*
         * checks if the point (x,y) is included in the Parallelepipede
         */

        public boolean includePoint(float x, float y) {
            Point p = new Point((int) x, (int) y);
            double d1 = distLineToPoint(points[0], points[1], p);
            double d2 = distLineToPoint(points[2], points[3], p);
            double range = distLineToPoint(points[0], points[1], points[2]);
            TctLog.d(TAG, "includePoint: " + d1 + " " + d2 + " " + range);
            /*
             * to be included in the shape, the distance from (x,y) to the
             * bottom or top line should not exceed the distance between the
             * bottom to top line
             */
            if (Math.max(d1, d2) < range) {
                return true;
            }
            return false;
        }

        /* computes the shortest distance form a point to a line */

        private double distLineToPoint(Point A, Point B, Point p) {

            /*
             * let [AB] be the segment and C the projection of C on (AB) AC * AB
             * (Cx-Ax)(Bx-Ax) + (Cy-Ay)(By-Ay) u = ------- =
             * ------------------------------- ||AB||^2 ||AB||^2
             */
            double det = Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2);
            if (det == 0) {
                return 0;
            }

            double u = ((p.x - A.x) * (B.x - A.x) + (p.y - A.y) * (B.y - A.y))
                    / det;

            /*
             * The projection point P can then be found: Px = Ax + r(Bx-Ax) Py =
             * Ay + r(By-Ay)
             */
            double Px = A.x + u * (B.x - A.x);
            double Py = A.y + u * (B.y - A.y);

            // TctLog.d(TAG,"distLineToPoint : u="+u+" Px=" +Px +" Py=" +Py);

            /* the distance to (AB) is the the [Pp] segment length */

            double distance = Math.sqrt(Math.pow(Px - p.x, 2)
                    + Math.pow(Py - p.y, 2));

            return distance;
        }

    }/* Parallelepipede */

}
