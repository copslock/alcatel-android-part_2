/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                          SpeakerTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.SystemClock;
import android.util.TctLog;
import android.view.KeyEvent;
import android.view.View;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

class SpeakerTest extends Test {

    private TestLayout1 tl = null;
    private Process p;
    private JRDRapi remote;
    private MediaPlayer mMediaPlayer;
    private AudioManager am;

    SpeakerTest(ID pid, String s) {
        this(pid, s, 0);
    }

    SpeakerTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                return true;
            }

        };

    }

    void setDataSourceFromResource(Resources resources, MediaPlayer player,
            int res) throws java.io.IOException {
        AssetFileDescriptor afd = resources.openRawResourceFd(res);
        if (afd != null) {
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            afd.close();
        }
    }

    private void startSpeaker() throws java.io.IOException,
            IllegalArgumentException, IllegalStateException {
        // player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.prepare();
        mMediaPlayer.start();

    }

    View.OnClickListener LeftButton = new View.OnClickListener() {
        public void onClick(View v) {
            mState = END;
            mMediaPlayer.stop();
            ExecuteTest.currentTest.Result = Test.PASSED;
            ExecuteTest.currentTest.Exit();
        }
    };

    View.OnClickListener RightButton = new View.OnClickListener() {
        public void onClick(View v) {
            mMediaPlayer.stop();
            ExecuteTest.currentTest.Result = Test.FAILED;
            ExecuteTest.currentTest.Exit();
        }
    };

    @SuppressWarnings("deprecation")
    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT:
            // mMediaPlayer = new MediaPlayer();
            // mMediaPlayer.setVolume(11, 11);
            mTimeIn.start();

            am = (AudioManager) mContext
                    .getSystemService(Context.AUDIO_SERVICE);

            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setVolume(11.0f, 11.0f);

            am.setStreamVolume(AudioManager.STREAM_MUSIC,
                    am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            am.setMode(AudioSystem.MODE_NORMAL);

            am.setSpeakerphoneOn(true);
            am.setWiredHeadsetOn(false);

            try {
                // ajayet : replaced the resource because quick gets
                // PVMFErrNotSupported from driver */

                setDataSourceFromResource(mContext.getResources(),
                        mMediaPlayer, R.raw.godfather_theme_slash_short/*
                                                                        * R.raw.
                                                                        * quick
                                                                        */);

                // mMediaPlayer.setDataSource(mContext.getFileStreamPath("samplefile.amr").getAbsolutePath());

                startSpeaker();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            tl = new TestLayout1(mContext, mName, "Speaker Melody test",
                    "PASS", "FAIL", LeftButton, RightButton);
            // if(!mTimeIn.isFinished())
            // tl.setAutoTestButtons(false);
            // tl.setAutoTestButtons(true);

            mContext.setContentView(tl.ll);

            break;

        case END:
            if (p != null) {
                p.destroy();
            }

            if (mMediaPlayer != null)
                mMediaPlayer.release();

            /* restore normal audio state to avoid affecting other applications */
            am.setStreamMute(AudioManager.STREAM_MUSIC, false);
            am.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
            am.setStreamMute(AudioManager.STREAM_RING, false);
            am.setMode(AudioSystem.MODE_NORMAL);

            break;
        default:
        }
    }

}

class MelodyTest extends Test {

    private TestLayout1 tl = null;

    private Process p;

    private JRDRapi remote;

    private boolean mLoopOn = false;

    private MediaPlayer mMediaPlayer;

    private AudioManager am;

    MelodyTest(ID pid, String s) {
        this(pid, s, 0);
    }

    MelodyTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                return true;
            }

        };

    }

    void setDataSourceFromResource(Resources resources, MediaPlayer player,
            int res) throws java.io.IOException {
        AssetFileDescriptor afd = resources.openRawResourceFd(res);
        if (afd != null) {
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            afd.close();
        }
    }

    private void startMelody() throws java.io.IOException,
            IllegalArgumentException, IllegalStateException {
        // player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.prepare();
        mMediaPlayer.start();

    }

    View.OnClickListener LeftButton = new View.OnClickListener() {
        public void onClick(View v) {
            mState++;
            Run();
        }
    };

    View.OnClickListener RightButton = new View.OnClickListener() {
        public void onClick(View v) {
            mMediaPlayer.stop();
            ExecuteTest.currentTest.Result = Test.FAILED;
            ExecuteTest.currentTest.Exit();
        }
    };

    @SuppressWarnings("deprecation")
    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT:

            mTimeIn.start();

            am = (AudioManager) mContext
                    .getSystemService(Context.AUDIO_SERVICE);

            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setVolume(11.0f, 11.0f);
            /* add by stephen.huang */
            am.setMode(AudioManager.MODE_IN_CALL);
            /* end add */
            am.setSpeakerphoneOn(false);
            am.setWiredHeadsetOn(false);
            // am.setMode(AudioSystem.ROUTE_SPEAKER);

            try {
                setDataSourceFromResource(mContext.getResources(),
                        mMediaPlayer, R.raw.in_call_alarm);
                startMelody();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            tl = new TestLayout1(mContext, mName, "Receiver discrete test",
                    "PASS", "FAIL", LeftButton, RightButton);

            // if(!mTimeIn.isFinished())
            // tl.setAutoTestButtons(false);
            // tl.setAutoTestButtons(true);

            mContext.setContentView(tl.ll);

            break;
        case INIT + 1:
            // mMediaPlayer = new MediaPlayer();
            // mMediaPlayer.setVolume(11, 11);
            mTimeIn.start();

            mMediaPlayer.reset();
            mMediaPlayer.setVolume(11.0f, 11.0f);

            am.setStreamVolume(AudioManager.STREAM_MUSIC,
                    am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            am.setMode(AudioSystem.MODE_NORMAL);

            am.setSpeakerphoneOn(true);
            am.setWiredHeadsetOn(false);

            try {
                // ajayet : replaced the resource because quick gets
                // PVMFErrNotSupported from driver */

                setDataSourceFromResource(mContext.getResources(),
                        mMediaPlayer, R.raw.godfather_theme_slash_short/*
                                                                        * R.raw.
                                                                        * quick
                                                                        */);

                // mMediaPlayer.setDataSource(mContext.getFileStreamPath("samplefile.amr").getAbsolutePath());

                startMelody();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            /*
             * fix venus_bug [153512], remove mic loop test in non-mini software
             * test, start
             */
            // if(!BuildConfig.isSW)
            // {
            // tl = new
            // TestLayout1(mContext,mName,"Speaker Melody test","PASS","FAIL",LeftButton,RightButton);
            // }
            // else
            // {
            // tl = new
            // TestLayout1(mContext,mName,"Speaker Melody test","PASS","FAIL");
            // }
            /*
             * fix venus_bug [153512], remove mic loop test in non-mini software
             * test, end
             */

            // if(!mTimeIn.isFinished())
            // {
            // tl.setAutoTestButtons(false);
            // tl.setAutoTestButtons(true);
            // }

            mContext.setContentView(tl.ll);

            break;

        case INIT + 2:

            mMediaPlayer.stop();
            mMediaPlayer.release();
            /* restore normal audio state to avoid affecting other applications */
            // am.setMode(AudioSystem.MODE_IN_CALL);
            SystemClock.sleep(1000);

            try {
                remote = new JRDRapi(mContext);
                remote.setAudioHandsetLoop(false);
                // remote.setAudioDualMicHandsetLoop(false);
                SystemClock.sleep(1500);// required to let the first command
                                        // finish

                remote.setAudioHandsetLoop(true);
                // remote.setAudioDualMicHandsetLoop(true);
                mLoopOn = true;

            } catch (FileNotFoundException e) {
                tl = new TestLayout1(mContext, mName,
                        "loop from MIC test init failed");
                mContext.setContentView(tl.ll);
            }
            tl = new TestLayout1(mContext, mName, "loop from MIC test");
            // if(!mTimeIn.isFinished())
            // tl.setAutoTestButtons(false);
            mContext.setContentView(tl.ll);

            break;

        case END:
            if (p != null) {
                p.destroy();
            }

            if (mLoopOn) {
                remote.setAudioHandsetLoop(false);
                // remote.setAudioDualMicHandsetLoop(false);
                mLoopOn = false;

                SystemClock.sleep(2000);
            }
            if (mMediaPlayer != null)
                mMediaPlayer.release();

            /* restore normal audio state to avoid affecting other applications */
            am.setStreamMute(AudioManager.STREAM_MUSIC, false);
            am.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
            am.setStreamMute(AudioManager.STREAM_RING, false);
            am.setMode(AudioSystem.MODE_NORMAL);

            break;
        default:
        }
    }

    @Override
    protected void onTimeInFinished() {
        // if(tl != null)
        // tl.setAutoTestButtons(true);
    }

}

class AudioLoopTest extends Test {

    private TestLayout1 tl = null;

    private Process p;

    private JRDRapi remote;

    private boolean mLoopOn = false;

    AudioLoopTest(ID pid, String s) {
        this(pid, s, 0);
    }

    AudioLoopTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                return true;
            }

        };

    }

    @SuppressWarnings("deprecation")
    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT:

            AudioManager am = (AudioManager) mContext
                    .getSystemService(Context.AUDIO_SERVICE);

            MediaPlayer mMediaPlayer = new MediaPlayer();

            mMediaPlayer.setVolume(11.0f, 11.0f);

            am.setMode(AudioManager.MODE_IN_CALL);
            am.setSpeakerphoneOn(false);
            am.setWiredHeadsetOn(false);

            try {
                AssetFileDescriptor afd = mContext.getResources()
                        .openRawResourceFd(R.raw.in_call_alarm);
                if (afd != null) {
                    mMediaPlayer.setDataSource(afd.getFileDescriptor(),
                            afd.getStartOffset(), afd.getLength());
                    afd.close();
                }
                mMediaPlayer.setLooping(false);
                mMediaPlayer.prepare();
                mMediaPlayer.start();

                while (mMediaPlayer.isPlaying()) {
                    ;
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                TctLog.d(TAG, "ERROR" + e);
            }
            mMediaPlayer.stop();

            // mMediaPlayer=null;

            // System.gc();

            SystemClock.sleep(2000);

            try {
                remote = new JRDRapi(mContext);
                remote.setAudioHandsetLoop(false);
                TctLog.e("ddddddd", "setAudioHandsetLoop");
                // remote.setAudioDualMicHandsetLoop(false);
                SystemClock.sleep(1000);// required to let the first command
                                        // finish
                remote.setAudioHandsetLoop(true);
                // remote.setAudioDualMicHandsetLoop(true);
                mLoopOn = true;
                tl = new TestLayout1(mContext, mName, "loop from MIC test");
                mContext.setContentView(tl.ll);

            } catch (FileNotFoundException e) {
                tl = new TestLayout1(mContext, mName,
                        "loop from MIC test init failed");
                mContext.setContentView(tl.ll);
            }

            break;

        case END:
            if (p != null) {
                p.destroy();
            }

            if (mLoopOn) {
                remote.setAudioHandsetLoop(false);
                // remote.setAudioDualMicHandsetLoop(false);
                mLoopOn = false;

                SystemClock.sleep(2000);
            }

            break;
        default:
        }
    }

    @Override
    protected void onTimeInFinished() {
        // if(tl != null)
        // tl.setAutoTestButtons(true);
    }

}

class MicTest extends Test {
    static final String SAMPLE_PREFIX = "recording";
    public static final int SDCARD_ACCESS_ERROR = 1;
    File mSampleFile = null;
    private MediaPlayer mMediaPlayer;
    private String TAG = Test.TAG + "MicTest";

    MediaRecorder recorder;

    MicTest(ID pid, String s) {
        super(pid, s);

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                return true;
            }

        };
    }

    void setDataSourceFromResource(Resources resources, MediaPlayer player,
            int res) throws java.io.IOException {
        AssetFileDescriptor afd = resources.openRawResourceFd(res);
        if (afd != null) {
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            afd.close();
        }
    }

    private void startMelody(MediaPlayer player) throws java.io.IOException,
            IllegalArgumentException, IllegalStateException {
        // player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setLooping(true);
        player.prepare();
        player.start();

    }

    private void startRecord() throws IllegalStateException, IOException {
        if (mSampleFile == null) {
            File sampleDir = Environment.getDataDirectory();
            if (!sampleDir.canWrite()) // Workaround for broken sdcard support
                                       // on the device.
                sampleDir = new File("/sdcard/sdcard");

            // FIXME:root permission issue.
            mSampleFile = mContext.getFileStreamPath("mictest.amr");
            // mSampleFile =
            // mContext.getFileStreamPath("/data/data/com.mmitest.mictest.amr");
            mSampleFile.createNewFile();
            mSampleFile.canRead();

            // the source file created by MMITest will be --rw------ by default
            // as MMITest has root rights
            // to let the player application work properly, file must have
            // -rw-rw-rw- rights.
            // so we change the file rights here using chmod call

            try {
                Process p = Runtime.getRuntime().exec(
                        "chmod 777 " + mSampleFile.getPath());
                p.waitFor();
                p = Runtime.getRuntime().exec("ls -l " + mSampleFile.getPath());
                p.waitFor();

                BufferedReader reader = new BufferedReader(
                        new java.io.InputStreamReader(p.getInputStream()));
                String line = reader.readLine();
                TctLog.i(TAG, "output file rights changed to: " + line);

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                TctLog.e(TAG, "can't change file rights");
            }

        }

        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        // recorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(mSampleFile.getAbsolutePath());
        recorder.prepare();
        recorder.start(); // Recording is now started
    }

    public void delete() {

        if (mSampleFile != null) {
            mSampleFile.delete();
        }
        mSampleFile = null;

    }

    @SuppressWarnings("deprecation")
    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {

        case INIT:

            mMediaPlayer = new MediaPlayer();

            recorder = new MediaRecorder();
            try {
                startRecord();
            } catch (IllegalStateException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            View.OnClickListener RButton = new View.OnClickListener() {
                public void onClick(View v) {
                    Result = Test.FAILED;
                    recorder.stop();
                    recorder.reset();

                    recorder.release();
                    recorder = null;

                    Exit();
                }
            };

            View.OnClickListener LButton = new View.OnClickListener() {
                public void onClick(View v) {
                    mState++;
                    recorder.stop();
                    recorder.reset();

                    recorder.release();
                    recorder = null;
                    Run();

                }
            };
            TestLayout1 t2 = new TestLayout1(mContext, mName,
                    "Start to record sound.pls speak loudly", "Stop and play",
                    "Fail", LButton, RButton);
            mContext.setContentView(t2.ll);

            break;
        case INIT + 1:

            mMediaPlayer.setVolume(11, 11);
            AudioManager am = (AudioManager) mContext
                    .getSystemService(Context.AUDIO_SERVICE);

            am.setSpeakerphoneOn(true);
            am.setWiredHeadsetOn(false);

            // SystemClock.sleep(1000);

            try {
                mMediaPlayer.setDataSource(mSampleFile.getAbsolutePath());
                startMelody(mMediaPlayer);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            TestLayout1 tl = new TestLayout1(mContext, mName,
                    "Can you hear what you said?");
            mContext.setContentView(tl.ll);
            mState++;

            break;

        case END:
            // Now the object cannot be reused
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            // delete();

            break;
        default:
        }
    }

}
