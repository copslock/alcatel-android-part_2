/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                        CommandManager.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/01/10| Qianbo.Pan     |     FR-378099      | Firmware version display   */
/* 13/06/10| Qianbo.Pan     |     PR-466129      | Firmware version display   */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.util.TctLog;

public class CommandManager {
    public static final String LOG = "CommandManager";
    public static final String FIRMWARE_FILE = "/sys/class/firmware_ver/device/firm_ver";
    public static  String BOARD_INFO = getTctBoardinfoPath("/sys/devices");
    private static CommandManager mManager;
    private FileInputStream fileInputStream = null;

    public static CommandManager getInstance() {
        if (mManager == null) {
            mManager = new CommandManager();
        }
        return mManager;
    }

    public CommandManager() {
    }

    public FileInputStream fileInputOpen(String path) {
        FileInputStream fis = null;

        try {
            File file = new File(path);
            if(!file.exists())
                throw new FileNotFoundException();
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            TctLog.e(LOG, "openInputStream error");
            e.printStackTrace();
            return fis;
        }
        return fis;
    }

    public byte[] readFileInputStream() {
        byte[] range = new byte[70];
        int ret = -1;
        try {
            ret = fileInputStream.read(range);
            if (ret == -1) {
                return null;
            }
        } catch (IOException e) {
            TctLog.e(LOG, "InputStream read error");
            e.printStackTrace();
            return null;
        }

        return range;
    }

    public void fileInputClose() {
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
                fileInputStream = null;
            } catch (IOException e) {
                TctLog.e(LOG, "inputStreamClose error");
                e.printStackTrace();
            }
        }
    }

    public static String getTctBoardinfoPath(String parentdir){
      File dir = new File(parentdir);
      String path=parentdir;
      String result = "/sys/devices/qpnp-vadc-ead05600/tct_board_info";
      if(!dir.exists()) {
          TctLog.e(LOG, "open dir:%s error"+ parentdir);
       }
      File files[]=dir.listFiles();
      for(File subfile:files){
        if(subfile.isDirectory() && subfile.getAbsolutePath().contains("qpnp-vadc-")){
          path = subfile.getAbsolutePath();
           break;
         }
       }
       if(path.contains("qpnp-vadc-")){
           File dir1 = new File(path);
            if(!dir1.exists()) {
                  TctLog.e(LOG, "open dir:%s error"+ path);
              }
           File files1[]=dir1.listFiles();
           for(File subfile:files1){
                if(!subfile.isDirectory() && subfile.getName().trim().equals("tct_board_info")){
                  result = subfile.getAbsolutePath();
                  break;
                  }
             }
        }else{
              TctLog.e(LOG, "can not find tct_board_info");
         }
         return result;
    }


    public void setFileInputStream(FileInputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }
}
