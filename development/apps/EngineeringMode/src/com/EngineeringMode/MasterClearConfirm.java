/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                   MasterClearConfirm.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Intent;
import android.view.View;

public class MasterClearConfirm extends Test {

    MasterClearConfirm(ID pid, String s) {
        super(pid, s);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void Run() {
        // TODO Auto-generated method stub
        switch (mState) {
        case INIT:
            TestLayout1 tl = new TestLayout1(mContext, mName,
                    "Factory data reset", "Reset", "Back", LeftButton,
                    RightButton);
            mContext.setContentView(tl.ll);
            break;

        default:
            break;
        }
    }

    View.OnClickListener LeftButton = new View.OnClickListener() {
        public void onClick(View v) {
            mContext.sendBroadcast(new Intent(
                    "android.intent.action.MASTER_CLEAR"));
        }
    };

    View.OnClickListener RightButton = new View.OnClickListener() {
        public void onClick(View v) {
            mContext.finish();
        }
    };

}
