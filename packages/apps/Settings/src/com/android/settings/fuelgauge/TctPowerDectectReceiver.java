package com.android.settings.fuelgauge;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class TctPowerDectectReceiver extends BroadcastReceiver{

    private static final String TAG = "TctPowerDectectReceiver";
    private static final String ACTION_POWER_DETECT_CHANGE = "android.intent.action.tct.POWER_NOTIFICATION_ACTION";
    private static final String POWER_DETECT_CHANGE_KEY = "power_consumption_notification_state";
    private static final String ACTION_POWER_DETECT_SERVICE = "com.android.settings.TCT_POWER_DETECT_SERVICE";

    private static final String ACTION_GET_HIGH_POWER_APP_LIST_SYNC = "android.intent.action.tct.GET_HIGH_POWER_APP_LIST_SYNC";
    private static final String ACTION_HIGH_POWER_DETECT_SYNC = "com.android.settings.TCT_HIGH_POWER_DETECT_SYNC";

    @Override
    public void onReceive(Context content, Intent intent) {

        Log.i(TAG, "intent = "+intent.getAction());
        String action = intent.getAction();
        if (action.equals(ACTION_POWER_DETECT_CHANGE)) {
            //Process intent with power defect status switch
            //key value: 0 is all off, 1 is only defect high power, 2 is only defect abnormal power, 3 is all on.
            Intent service = new Intent(ACTION_POWER_DETECT_SERVICE);
            service.putExtra(POWER_DETECT_CHANGE_KEY, intent.getIntExtra(POWER_DETECT_CHANGE_KEY,0));
            service.setPackage("com.android.settings");

            content.startService(service);
        } else if(action.equals(ACTION_GET_HIGH_POWER_APP_LIST_SYNC)) {
            //Process with intent to get high power apps right now
            Intent service = new Intent(ACTION_HIGH_POWER_DETECT_SYNC);
            service.setPackage("com.android.settings");
            content.startService(service);
        }
    }
}
