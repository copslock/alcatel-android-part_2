package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.android.internal.telephony.TelephonyIntents.SECRET_CODE_ACTION;

import com.android.settings.Settings.TestingSettingsActivity;


public class TestingSettingsBroadcastReceiver extends BroadcastReceiver {
  
    public TestingSettingsBroadcastReceiver() {
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        //[SOLUTION]-Mod-BEGIN by TCTNB.(Caixia Chen), 08/1/2016, SOLUTION-2521249
        boolean isEngModOn = context.getResources().getBoolean(R.bool.feature_settings_engmod_on);
        if (isEngModOn && intent.getAction().equals(SECRET_CODE_ACTION)) {
            //[SOLUTION]-Mod-END by TCTNB.(Caixia Chen)
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setClass(context, TestingSettingsActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
