/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.aod;

/* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
import com.android.settings.CustomEditTextPreference;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
import android.provider.Settings;
/* MODIFIED-BEGIN by song.huan, 2016-10-13,BUG-3000498*/
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.EditTextPreference;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
/* MODIFIED-END by song.huan,BUG-3000498*/
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
/* MODIFIED-END by song.huan,BUG-3000498*/
import android.widget.Switch;
import android.widget.TextView;

import com.android.settings.R;
import com.android.settings.widget.SwitchBar;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;

import static android.provider.Settings.System.AOD_SIGNATURE_ENABLED;
/* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
import static android.provider.Settings.System.AOD_SIGNATURE_STYLE;

public class AlwaysOnDisplaySetSignaturePreference extends CustomEditTextPreference{


    private Context mCnotext;
    private SwitchBar mSwitchBar;
    private BroadcastReceiver mReceiver;

    //private TextView mSignatureShow;
    private EditText mEditSignature;
    private TextView mSignatureCancel;
    private TextView mSignatureSave;
    private final int MAX_SIGNATURE_LENGTH = 18; // MODIFIED by song.huan, 2016-10-13,BUG-3000498

    public AlwaysOnDisplaySetSignaturePreference(Context context,
            AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
       setDialogLayoutResource(R.layout.signature_style_dialog);
        mCnotext = context;

    }


    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        final EditText editText = (EditText) view.findViewById(android.R.id.edit);
        /* MODIFIED-BEGIN by song.huan, 2016-10-13,BUG-3000498*/
        TextInputLayout inputLayout = (TextInputLayout) view.findViewById(R.id.editLayout);
        inputLayout.setCounterMaxLength(MAX_SIGNATURE_LENGTH);
        /* MODIFIED-END by song.huan,BUG-3000498*/
        initEditText(editText);

    }

    public void SaveContent(String value) {
        boolean isNone = true;
        if(!value.equals("")){ isNone = false;}
        final Context context = this.getContext();
        Settings.System.putInt(context.getContentResolver(), AOD_SIGNATURE_ENABLED, isNone ? 0 : 1);
        Settings.System.putString(context.getContentResolver(),AOD_SIGNATURE_STYLE,value);
    }

    /* MODIFIED-BEGIN by song.huan, 2016-10-13,BUG-3000498*/
    public void initEditText(final EditText editText){

        if (editText != null) {
            editText.setInputType(InputType.TYPE_CLASS_TEXT);
            final Context context = this.getContext();
            String value = Settings.System.getString(context.getContentResolver(), AOD_SIGNATURE_STYLE);
            if(value == null || value.equals("")){editText.setHint(R.string.always_on_signature_preview);}
            else {editText.setText(value);}
            editText.setSelection(editText.getText().length());
            editText.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                    int count, int after) {
                    // TODO Auto-generated method stub
               }

                @Override
                public void onTextChanged(CharSequence s, int start,
                        int before, int count) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub
                    onEditTextChanged();
   }

            });
            /* MODIFIED-END by song.huan,BUG-3000498*/
        }
    }


    @Override
    protected void onPrepareDialogBuilder(Builder builder,
            android.content.DialogInterface.OnClickListener listener) {
        // TODO Auto-generated method stub
        super.onPrepareDialogBuilder(builder, listener);
        /* MODIFIED-BEGIN by song.huan, 2016-10-13,BUG-3000498*/
        //custom the buttons of alertDialog
        builder.setPositiveButton(R.string.always_on_signature_positive_btn, listener)
               .setNegativeButton(R.string.always_on_signature_negative_btn, listener);
    }


    private boolean isSignatureValid(String value) {
        return value.length() <= MAX_SIGNATURE_LENGTH ? true : false;
    }

    private void onEditTextChanged(){
        //when the signature which user input is longer then the max length
        //set the positive button disabled
        boolean enable = isSignatureValid(getEditText().getText().toString());
        Dialog mDialog = getDialog();
        if(mDialog instanceof AlertDialog){
            AlertDialog editDialog = (AlertDialog) mDialog;
            editDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(enable);
        }
    }
    /* MODIFIED-END by song.huan,BUG-3000498*/


}
