/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.aod;

/* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
import com.android.settings.CustomEditTextPreference;
import com.android.settings.R;
import com.android.settings.SettingsActivity;
import com.android.settings.SettingsPreferenceFragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
/* MODIFIED-END by song.huan,BUG-3000498*/
import android.os.Bundle;

import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.widget.SwitchBar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText; // MODIFIED by song.huan, 2016-10-11,BUG-3000498
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import static android.provider.Settings.Secure.AOD_ENABLED;
import static android.provider.Settings.System.AOD_CLOCK_ENABLED;
import static android.provider.Settings.System.AOD_BG_ENABLED;
import static android.provider.Settings.System.AOD_SIGNATURE_ENABLED;
import static android.provider.Settings.System.AOD_BG_STYLE;
import static android.provider.Settings.System.AOD_COLCK_STYLE;
import static android.provider.Settings.System.AOD_SIGNATURE_STYLE;

import android.provider.Settings;

/* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
public class AlwaysOnDisplaySettings extends SettingsPreferenceFragment implements
        Preference.OnPreferenceChangeListener{
        /* MODIFIED-END by song.huan,BUG-3000498*/

    private SwitchBar mSwitchBar;

    private boolean isFirstLaunch = true;

    private static final String KEY_AOD_CLOCK_STYLE = "aod_clock_style";
    private static final String KEY_AOD_SIGNATURE_STYLE = "aod_signature_style";
    private static final String KEY_AOD_BG_STYLE = "aod_background_style";
    private static final String KEY_AOD_PREVIEW = "aod_preview";

    private Preference mAlwaysOnDisplayClockStylePreference;
    private AlwaysOnDisplaySetSignaturePreference mAlwaysOnDisplaySignatureStylePreference; // MODIFIED by song.huan, 2016-10-11,BUG-3000498
    private Preference mAlwaysOnDisplayBackgroundStylePreference;
    private AlwaysDisplayPreference mAlwaysOnDisplayPreviewPreference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void onBindPreferences() {
        // TODO Auto-generated method stub
        super.onBindPreferences();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("edward", "enter onCreat");
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.always_on_display_settings);

        mAlwaysOnDisplayClockStylePreference = findPreference(KEY_AOD_CLOCK_STYLE);
        /* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
        mAlwaysOnDisplaySignatureStylePreference = (AlwaysOnDisplaySetSignaturePreference)findPreference(KEY_AOD_SIGNATURE_STYLE);
        mAlwaysOnDisplayBackgroundStylePreference = findPreference(KEY_AOD_BG_STYLE);
        mAlwaysOnDisplayPreviewPreference = (AlwaysDisplayPreference) findPreference(KEY_AOD_PREVIEW);
        mAlwaysOnDisplaySignatureStylePreference.setOnPreferenceChangeListener(this);
        /* MODIFIED-END by song.huan,BUG-3000498*/
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initSwitchBar();
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.QS_BLUETOOTH;
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        isFirstLaunch = false;

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        updateClockSummary();
        updateSignatureSummary();
        updateBackgroundSummary();
        if(!isFirstLaunch){
            updatePreview();
        }

    /* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (mAlwaysOnDisplaySignatureStylePreference == preference) {
        	mAlwaysOnDisplaySignatureStylePreference.SaveContent((String) newValue);
        	updatePreview();
        	updateSignatureSummary();
            return true;
        }
        return false;
    }


    private int[] clockStyles = {R.string.always_on_clock_style_1, R.string.always_on_clock_style_2,
            R.string.always_on_clock_style_3, R.string.always_on_clock_style_4,
            R.string.always_on_clock_style_5, R.string.always_on_clock_style_6,
            R.string.always_on_clock_style_7,};

    private int[] BackgroundStyles = {R.string.always_on_bg_style_1, R.string.always_on_bg_style_2,
            R.string.always_on_bg_style_3, R.string.always_on_bg_style_4,
            R.string.always_on_bg_style_5, R.string.always_on_bg_style_6};

    private void updateClockSummary(){
        final Context context = mAlwaysOnDisplayClockStylePreference.getContext();
        int value = Settings.System.getInt(context.getContentResolver(),AOD_CLOCK_ENABLED, 1);
        int savedStyle = Settings.System.getInt(context.getContentResolver(),AOD_COLCK_STYLE, 0);
        mAlwaysOnDisplayClockStylePreference.setSummary(value == 1 ? clockStyles[savedStyle] : R.string.always_on_display_summary_off);
    }

    private void updateSignatureSummary(){
        final Context context = mAlwaysOnDisplaySignatureStylePreference.getContext();
        int value = Settings.System.getInt(context.getContentResolver(),AOD_SIGNATURE_ENABLED, 0);
        String savedSignature = Settings.System.getString(context.getContentResolver(), AOD_SIGNATURE_STYLE);
        if(value == 1){
            mAlwaysOnDisplaySignatureStylePreference.setSummary(savedSignature);
        }else {
            mAlwaysOnDisplaySignatureStylePreference.setSummary(R.string.always_on_display_summary_off);
        }


    }

    private void updateBackgroundSummary(){
        final Context context = mAlwaysOnDisplayBackgroundStylePreference.getContext();
        int value = Settings.System.getInt(context.getContentResolver(),AOD_BG_ENABLED, 0);
        int savedStyle = Settings.System.getInt(context.getContentResolver(),AOD_BG_STYLE, 0);
        mAlwaysOnDisplayBackgroundStylePreference.setSummary(value == 1 ? BackgroundStyles[savedStyle] : R.string.always_on_display_summary_off);
        /* MODIFIED-END by song.huan,BUG-3000498*/
    }

    private void updatePreview(){
    	mAlwaysOnDisplayPreviewPreference.initView();
    }



    private void initSwitchBar() {
        final SettingsActivity sa = (SettingsActivity) getActivity();
        mSwitchBar = sa.getSwitchBar();
        mSwitchBar
                .addOnSwitchChangeListener(new SwitchBar.OnSwitchChangeListener() {

                    @Override
                    public void onSwitchChanged(Switch switchView,
                            boolean isChecked) {
                        // TODO Auto-generated method stub
                        Settings.Secure.putInt(getContentResolver(),
                                AOD_ENABLED, isChecked ? 1 : 0);
                    }

                });
        mSwitchBar.show();
        // init switchbar checked
        if (mSwitchBar != null) {
            int value = Settings.Secure.getInt(getContentResolver(),
                    AOD_ENABLED, 0);
            mSwitchBar.setChecked(value == 1);
            Log.d("edward", "value--->" + value);
        }
    }

}
