/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C)  The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.os.SystemProperties;
import java.util.Locale;

import com.android.internal.telephony.TelephonyIntents;

//Add by fei.liu for defect-1924723,04/14/2016, begin
public class LocaleChangeReceiver extends BroadcastReceiver {

    private static final String TAG = "LocaleChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (TelephonyIntents.ACTION_CURRENT_LOCALE_CHANGED.equals(intent.getAction())) {
            String newLanguage = intent.getStringExtra("current_language");
            String newCountry = intent.getStringExtra("current_country");
            Log.d(TAG, "Received ACTION_CURRENT_LOCALE_CHANGED, newLanguage = " + newLanguage +
                ", newCountry = " + newCountry);

            SystemProperties.set("persist.sys.language", newLanguage);
            SystemProperties.set("persist.sys.country", newCountry);
        }
    }
}
//Add by fei.liu, end
