/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/09/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import android.content.Intent;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.widget.SwitchBar;
import com.android.settings.widget.ToggleSwitch;

public class PrivacyModeHelpFragment extends SettingsPreferenceFragment implements ToggleSwitch.OnBeforeCheckedChangeListener {
    private SwitchBar mSwitchBar;
    protected ToggleSwitch mToggleSwitch;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final SettingsActivity activity = (SettingsActivity) getActivity();

        mSwitchBar = activity.getSwitchBar();
        mToggleSwitch = mSwitchBar.getSwitch();
        mSwitchBar.setChecked(false);
        mToggleSwitch.setOnBeforeCheckedChangeListener(this);
        mSwitchBar.show();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup parent = (ViewGroup) view.findViewById(android.R.id.list_container);
        View emptyView = LayoutInflater.from(getContext())
                .inflate(R.layout.privacy_mode_help, parent, false);
        parent.addView(emptyView);
        setEmptyView(emptyView);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mToggleSwitch.setOnBeforeCheckedChangeListener(null);
        mSwitchBar.hide();
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.TCT_PRIVATE_MODE;
    }

    @Override
    public boolean onBeforeCheckedChanged(ToggleSwitch toggleSwitch,
            boolean checked) {
        mSwitchBar.setCheckedInternal(false);
        Intent intent = new Intent("com.android.settings.SET_PRIVACY_MODE");
        startActivity(intent);
        return true;
    }
}
