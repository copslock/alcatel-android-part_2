package com.mms.wrap;

import android.content.Context;
import android.util.Log;
import android.telephony.SmsManager;
import com.qcom.frameworks.cb.CBUtils;
import android.provider.Settings;


class WrapCommon implements IWrapCommon {

    protected boolean getBooleanFromPlf(Context context,String key) {
        try {
            return WrapUtils.getBooleanFromXml(key);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"getBooleanFromPlf error key:" + key);
        }
        return false;
    }

    @Override
    public void setCellBroadcastRangesEmpty() {
        CBUtils.setCellBroadcastRangesEmpty();
    }

    @Override
    public void getCellBroadcastConfig(SmsManager manager) {
        CBUtils.getCellBroadcastConfig(manager);
    }

    @Override
    public int getSettingSystemFlipMonitorValue(Context context) {
        int enabled = 0;
        try {
            //enabled = Settings.System.getInt(context.getContentResolver(),Settings.System.FLIP_OVER_MUTE, 0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"getSettingSystemFlipMonitorValue error");
        }
        return enabled;
    }

    @Override
    public int getSettingSystemMessageUserpinValue(Context context) {
        int enabled = 0;
        try {
            //enabled = Settings.System.getInt(context.getContentResolver(),Settings.System.OMA_CP_MESSAGE_USERPIN_ENABLE, 0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"getSettingSystemMessageUserpinValue error");
        }
        return enabled;
    }

    @Override
    public int getSettingSystemWapMessageValue(Context context) {
        int enabled = 0;
        try {
            enabled = Settings.System.getInt(context.getContentResolver(),Settings.System.TCT_WAP_MESSAGE_ENABLE, 0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"getSettingSystemWapMessageValue error");
        }
        return enabled;
    }

    @Override
    public void putSettingSystemFlipMonitorValue(Context context, int value) {
        try {
            //Settings.System.putInt(context.getContentResolver(), Settings.System.FLIP_OVER_MUTE, value);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"putSettingSystemFlipMonitorValue error");
        }
    }

    @Override
    public void putSettingSystemMessageUserpinValue(Context context, int value) {
        try {
            //Settings.System.putInt(context.getContentResolver(), Settings.System.OMA_CP_MESSAGE_USERPIN_ENABLE, value);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"putSettingSystemMessageUserpinValue error");
        }
    }

    @Override
    public void putSettingSystemWapMessageValue(Context context, int value) {
        try {
            Settings.System.putInt(context.getContentResolver(), Settings.System.TCT_WAP_MESSAGE_ENABLE, value);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"putSettingSystemWapMessageValue error");
        }
    }
}
