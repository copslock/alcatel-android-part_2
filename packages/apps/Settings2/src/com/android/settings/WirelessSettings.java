/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/15/2016|     Dandan.Fang      |     TASK2721235      |[Telecom_Interna- */
/*           |                      |                      |l[GL/CN]] VoLTE   */
/*           |                      |                      |contorl menu      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/30/2016|     Dandan.Fang      |     TASK2817990      |[London][Telecom- */
/*           |                      |                      |][CTCC][4G Switc- */
/*           |                      |                      |h]ADD 4G button   */
/*           |                      |                      |for CTCC          */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.settings;


import static android.net.ConnectivityManager.TETHERING_BLUETOOTH;
import static android.net.ConnectivityManager.TETHERING_USB;
import static android.net.ConnectivityManager.TETHERING_WIFI;
import android.app.Activity;
import mst.app.dialog.AlertDialog;
import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothPan;
import android.bluetooth.BluetoothProfile;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.usb.UsbManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.SearchIndexableResource;
import android.provider.Settings;
import mst.preference.SwitchPreference;
import mst.preference.Preference;
import mst.preference.PreferenceScreen;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.settings.datausage.DataSaverBackend;
import com.android.settings.nfc.NfcEnabler;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedPreference;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;






//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/30/2016,TASK2817990 ,
//[4G Switch]ADD 4G button for CTCC
import android.telephony.SubscriptionManager;
//[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.widget.CheckBox;
import android.content.SharedPreferences;
import android.widget.LinearLayout;
//[FEATURE]-Add-END by TCTNB.bo.chen
import com.android.internal.telephony.Phone;
import android.provider.Settings.SettingNotFoundException;
//[BUGFIX]-Add-END by TCTNB.Dandan.Fang

//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/15/2016,TASK2721235,
//[Telecom_Internal[GL/CN]] VoLTE contorl menu
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.Preference.OnPreferenceClickListener;
//[FEATURE]-Mod-BEGIN by TCTNB.dongdong.gong,10/25/2016,3082231,
//move the usb and bluetooth tether to another place.
public class WirelessSettings extends SettingsPreferenceFragment implements OnPreferenceChangeListener,Indexable,DataSaverBackend.Listener {
  //[FEATURE]-Mod-END by TCTNB.dongdong.gong
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
    private static final String TAG = "WirelessSettings";

    private static final String KEY_TOGGLE_AIRPLANE = "toggle_airplane";
    private static final String KEY_TOGGLE_BEST_NETWORK = "toggle_best_network";//[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
    private static final String KEY_TOGGLE_NFC = "toggle_nfc";
    private static final String KEY_WIMAX_SETTINGS = "wimax_settings";
    private static final String KEY_ANDROID_BEAM_SETTINGS = "android_beam_settings";
    private static final String KEY_VPN_SETTINGS = "vpn_settings";
    private static final String KEY_TETHER_SETTINGS = "tether_settings";
    private static final String KEY_PROXY_SETTINGS = "proxy_settings";
    private static final String KEY_MOBILE_NETWORK_SETTINGS = "mobile_network_settings";
    private static final String KEY_MANAGE_MOBILE_PLAN = "manage_mobile_plan";
    private static final String KEY_WFC_SETTINGS = "wifi_calling_settings";

    public static final String EXIT_ECM_RESULT = "exit_ecm_result";
    public static final int REQUEST_CODE_EXIT_ECM = 1;

    /**
     * Broadcast intent action when the location mode is about to change.
     */
    private static final String MODE_CHANGING_ACTION =
            "com.android.settings.location.MODE_CHANGING";
    private static final String CURRENT_MODE_KEY = "CURRENT_MODE";
    private static final String NEW_MODE_KEY = "NEW_MODE";

    private AirplaneModeEnabler mAirplaneModeEnabler;
    private SwitchPreference mAirplaneModePreference;
    //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
    private SwitchPreference mBestNetworkModePreference;
    private static final String BEST_NETWORK_TOAST = "bestNetworkToast";
    private LinearLayout noMoreReminderLayout;
    private int mSimCardNum;
    private boolean mAnyCard;
    private int mPhoneNum;
    BroadcastReceiver mReceiver;
    //[FEATURE]-Add-END by TCTNB.bo.chen
    private NfcEnabler mNfcEnabler;
    private NfcAdapter mNfcAdapter;

    private ConnectivityManager mCm;
    private TelephonyManager mTm;
    private PackageManager mPm;
    private UserManager mUm;
  //[FEATURE]-Mod-BEGIN by TCTNB.dongdong.gong,10/25/2016,3082231,
  //move the usb and bluetooth tether to another place.
    private String[] mUsbRegexs;
    private String[] mBluetoothRegexs;
    private BroadcastReceiver mTetherChangeReceiver;
    private boolean mBluetoothEnableForTether;
    private SwitchPreference mUsbTether;
    private SwitchPreference mBluetoothTether;
    private static final String USB_TETHER_SETTINGS = "usb_tether_settings";
    private static final String ENABLE_BLUETOOTH_TETHERING = "enable_bluetooth_tethering";
    private static final String DATA_SAVER_FOOTER = "disabled_on_data_saver";
    private AtomicReference<BluetoothPan> mBluetoothPan = new AtomicReference<BluetoothPan>();
    private OnStartTetheringCallback mStartTetheringCallback;
    private boolean mUsbConnected;
    private boolean mMassStorageActive;
    private Handler mHandler = new Handler();
    private boolean mDataSaverEnabled;
    private DataSaverBackend mDataSaverBackend;
    private Preference mDataSaverFooter;
    //[FEATURE]-Mod-END by TCTNB.dongdong.gong

    boolean mIsNetworkSettingsAvailable = false;

    private static final int MANAGE_MOBILE_PLAN_DIALOG_ID = 1;
    private static final int BEST_ACCESS_NETWORK_DIALOG_ID = 2;//[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
    private static final String SAVED_MANAGE_MOBILE_PLAN_MSG = "mManageMobilePlanMessage";

    private PreferenceScreen mButtonWfc;
    /* MODIFIED-BEGIN by Dandan.FANG@tcl.com, 2016-09-02,BUG-2817990*/
    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/15/2016,TASK2721235,
    //[Telecom_Internal[GL/CN]] VoLTE contorl menu
    private static final String BUTTON_4G_LTE_KEY = "enhanced_4g_lte";
    private SwitchPreference mButton4glte;
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

  //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/30/2016,TASK2817990 ,
  //[4G Switch]ADD 4G button for CTCC
    private static final String KEY_TOGGLE_LTE_SWITCH = "toggle_LTE_switch";
    private SwitchPreference mLteSwitchPreference;
    private int settingsNetworkMode = -1;
    private boolean mLteSwitchon;
  //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
  /* MODIFIED-END by Dandan.FANG@tcl.com,BUG-2817990*/

    //[FEATURE]-ADD_BEGIN by TSNJ.heng.zhang1,2016-09-23,Task-2827972
    private static final String KEY_TOGGLE_LOCATION = "toggle_location";
    private SwitchPreference mToggleLocation;
    private BroadcastReceiver mLocationSettingsReceiver;
    private int mCurrentLocationMode;
    private boolean mLocatioSettingsActive;
    //[FEATURE]-ADD_END by TSNJ.heng.zhang1,2016-09-23,Task-2827972
    /**
     * Invoked on each preference click in this hierarchy, overrides
     * PreferenceFragment's implementation.  Used to make sure we track the
     * preference click events.
     */
    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference)  {
        log("onPreferenceTreeClick: preference=" + preference);
        if (preference == mAirplaneModePreference && Boolean.parseBoolean(
                SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
            // In ECM mode launch ECM app dialog
            startActivityForResult(
                new Intent(TelephonyIntents.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS, null),
                REQUEST_CODE_EXIT_ECM);
            return true;
        } else if (preference == findPreference(KEY_MANAGE_MOBILE_PLAN)) {
            onManageMobilePlanClick();
        } else if (preference == findPreference(KEY_MOBILE_NETWORK_SETTINGS)
                && mIsNetworkSettingsAvailable) {
            onMobileNetworkSettingsClick();
            return true;
        }
      //[FEATURE]-Mod-BEGIN by TCTNB.dongdong.gong,10/25/2016,3082231,
      //move the usb and bluetooth tether to another place.
        if (preference == mUsbTether) {
            if (mUsbTether.isChecked()) {
                startTethering(TETHERING_USB);
            } else {
                mCm.stopTethering(TETHERING_USB);
            }
        } else if (preference == mBluetoothTether) {
            if (mBluetoothTether.isChecked()) {
                startTethering(TETHERING_BLUETOOTH);
            } else {
                mCm.stopTethering(TETHERING_BLUETOOTH);
                // No ACTION_TETHER_STATE_CHANGED is fired or bluetooth unless a device is
                // connected. Need to update state manually.
                updateState();
            }
        }
        //[FEATURE]-Mod-END by TCTNB.dongdong.gong
        // Let the intents be launched by the Preference manager
        return super.onPreferenceTreeClick(preferenceScreen,preference);
    }

    public void onMobileNetworkSettingsClick() {
        log("onMobileNetworkSettingsClick:");
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        log("qti MobileNetworkSettings Enabled");
        // prepare intent to start qti MobileNetworkSettings activity
        intent.setComponent(new ComponentName("com.qualcomm.qti.networksetting",
               "com.qualcomm.qti.networksetting.MobileNetworkSettings"));
        startActivity(intent);
    }
    private String mManageMobilePlanMessage;
    public void onManageMobilePlanClick() {
        log("onManageMobilePlanClick:");
        mManageMobilePlanMessage = null;
        Resources resources = getActivity().getResources();

        NetworkInfo ni = mCm.getActiveNetworkInfo();
        if (mTm.hasIccCard() && (ni != null)) {
            // Check for carrier apps that can handle provisioning first
            Intent provisioningIntent = new Intent(TelephonyIntents.ACTION_CARRIER_SETUP);
            List<String> carrierPackages =
                    mTm.getCarrierPackageNamesForIntent(provisioningIntent);
            if (carrierPackages != null && !carrierPackages.isEmpty()) {
                if (carrierPackages.size() != 1) {
                    Log.w(TAG, "Multiple matching carrier apps found, launching the first.");
                }
                provisioningIntent.setPackage(carrierPackages.get(0));
                startActivity(provisioningIntent);
                return;
            }

            // Get provisioning URL
            String url = mCm.getMobileProvisioningUrl();
            if (!TextUtils.isEmpty(url)) {
                Intent intent = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN,
                        Intent.CATEGORY_APP_BROWSER);
                intent.setData(Uri.parse(url));
                intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.w(TAG, "onManageMobilePlanClick: startActivity failed" + e);
                }
            } else {
                // No provisioning URL
                String operatorName = mTm.getSimOperatorName();
                if (TextUtils.isEmpty(operatorName)) {
                    // Use NetworkOperatorName as second choice in case there is no
                    // SPN (Service Provider Name on the SIM). Such as with T-mobile.
                    operatorName = mTm.getNetworkOperatorName();
                    if (TextUtils.isEmpty(operatorName)) {
                        mManageMobilePlanMessage = resources.getString(
                                R.string.mobile_unknown_sim_operator);
                    } else {
                        mManageMobilePlanMessage = resources.getString(
                                R.string.mobile_no_provisioning_url, operatorName);
                    }
                } else {
                    mManageMobilePlanMessage = resources.getString(
                            R.string.mobile_no_provisioning_url, operatorName);
                }
            }
        } else if (mTm.hasIccCard() == false) {
            // No sim card
            mManageMobilePlanMessage = resources.getString(R.string.mobile_insert_sim_card);
        } else {
            // NetworkInfo is null, there is no connection
            mManageMobilePlanMessage = resources.getString(R.string.mobile_connect_to_internet);
        }
        if (!TextUtils.isEmpty(mManageMobilePlanMessage)) {
            log("onManageMobilePlanClick: message=" + mManageMobilePlanMessage);
            showDialog(MANAGE_MOBILE_PLAN_DIALOG_ID);
        }
    }

    @Override
    public Dialog onCreateDialog(int dialogId) {
        log("onCreateDialog: dialogId=" + dialogId);
        switch (dialogId) {
            case MANAGE_MOBILE_PLAN_DIALOG_ID:
                return new AlertDialog.Builder(getActivity())
                            .setMessage(mManageMobilePlanMessage)
                            .setCancelable(false)
                            .setPositiveButton(com.android.internal.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    log("MANAGE_MOBILE_PLAN_DIALOG.onClickListener id=" + id);
                                    mManageMobilePlanMessage = null;
                                }
                            })
                            .create();
            //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
            case BEST_ACCESS_NETWORK_DIALOG_ID:
                noMoreReminderLayout = (LinearLayout)getActivity().getLayoutInflater().inflate(R.layout.nomore_reminder, null);
                return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.best_network)
                .setMessage(R.string.best_network_message)
                .setView(noMoreReminderLayout)
                .setCancelable(false)
                .setNegativeButton(com.android.internal.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        log("cancel BEST_ACCESS_NETWORK_DIALOG_ID.onClickListener id=" + id);
                        mBestNetworkModePreference.setChecked(false);
                        Settings.Global.putInt(getContentResolver(), "is_select_best_network", 0);
                    }
                })
                .setPositiveButton(com.android.internal.R.string.ok,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        log("BEST_ACCESS_NETWORK_DIALOG_ID.onClickListener id=" + id);
                        CheckBox checkBox = (CheckBox)noMoreReminderLayout.findViewById(R.id.nomore);
                        if(checkBox.isChecked()){
                            SharedPreferences sp = getActivity().getSharedPreferences(BEST_NETWORK_TOAST, Context.MODE_PRIVATE);
                            sp.edit().putBoolean("isHideToast", true).commit();
                        }
                        mBestNetworkModePreference.setChecked(true);
                        Settings.Global.putInt(getContentResolver(), "is_select_best_network", 1);
                    }
                })
                .create();
                //[FEATURE]-Add-END by TCTNB.bo.chen
        }
        return super.onCreateDialog(dialogId);
    }

    private void log(String s) {
        Log.d(TAG, s);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.WIRELESS;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mManageMobilePlanMessage = savedInstanceState.getString(SAVED_MANAGE_MOBILE_PLAN_MSG);
        }
        log("onCreate: mManageMobilePlanMessage=" + mManageMobilePlanMessage);

        mCm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mTm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mPm = getPackageManager();
        mUm = (UserManager) getSystemService(Context.USER_SERVICE);

        addPreferencesFromResource(R.xml.wireless_settings);

        final boolean isAdmin = mUm.isAdminUser();

        final Activity activity = getActivity();
        mAirplaneModePreference = (SwitchPreference) findPreference(KEY_TOGGLE_AIRPLANE);
        SwitchPreference nfc = (SwitchPreference) findPreference(KEY_TOGGLE_NFC);
        RestrictedPreference androidBeam = (RestrictedPreference) findPreference(
                KEY_ANDROID_BEAM_SETTINGS);

        mAirplaneModeEnabler = new AirplaneModeEnabler(activity, mAirplaneModePreference);
        mNfcEnabler = new NfcEnabler(activity, nfc, androidBeam);

        mButtonWfc = (PreferenceScreen) findPreference(KEY_WFC_SETTINGS);

      /* MODIFIED-BEGIN by Dandan.FANG@tcl.com, 2016-09-02,BUG-2817990*/
      //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/15/2016,TASK2721235,
      //[Telecom_Internal[GL/CN]] VoLTE contorl menu
        mButton4glte = (SwitchPreference)findPreference(BUTTON_4G_LTE_KEY);
        mButton4glte.setOnPreferenceChangeListener(this);
        mButton4glte.setChecked(ImsManager.isEnhanced4gLteModeSettingEnabledByUser(getActivity()));
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

        //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
        mBestNetworkModePreference = (SwitchPreference) findPreference(KEY_TOGGLE_BEST_NETWORK);
        mBestNetworkModePreference.setOnPreferenceChangeListener(this);
        boolean isBestAccess = Settings.Global.getInt(getContentResolver(), "is_select_best_network", 0) != 0;
        mBestNetworkModePreference.setChecked(isBestAccess);
        boolean isTurnOnBestNetwork = getResources().getBoolean(com.android.internal.R.bool.def_tctfw_isTurnOnBestNetworkAcess);
        if (!isTurnOnBestNetwork) {
            removePreference(KEY_TOGGLE_BEST_NETWORK);
        }
        //[FEATURE]-Add-END by TCTNB.bo.chen
      //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/30/2016,TASK2817990 ,
      //[4G Switch]ADD 4G button for CTCC
        mLteSwitchPreference = (SwitchPreference) findPreference(KEY_TOGGLE_LTE_SWITCH);
        mLteSwitchPreference.setOnPreferenceChangeListener(this);
        int mSubId = SubscriptionManager.from(getActivity()).getDefaultDataSubscriptionId();
        int mSlotId = SubscriptionManager.from(getActivity()).getSlotId(mSubId);
        try {
            settingsNetworkMode = android.telephony.TelephonyManager
                    .getIntAtIndex(getActivity().getContentResolver(),
                            android.provider.Settings.Global.PREFERRED_NETWORK_MODE,mSlotId);
        } catch (SettingNotFoundException snfe) {
            settingsNetworkMode = Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
        }
        log("WirelessSettings settingnetworkmode = " + settingsNetworkMode);
        boolean isLTE = false;
        if (settingsNetworkMode == Phone.NT_MODE_LTE_CDMA_AND_EVDO
                || settingsNetworkMode == Phone.NT_MODE_LTE_GSM_WCDMA
                || settingsNetworkMode == Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA
                || settingsNetworkMode == Phone.NT_MODE_LTE_ONLY
                || settingsNetworkMode == Phone.NT_MODE_LTE_WCDMA
                || settingsNetworkMode == Phone.NT_MODE_LTE_WCDMA
                || settingsNetworkMode == Phone.NT_MODE_LTE_TDSCDMA
                || settingsNetworkMode == Phone.NT_MODE_LTE_TDSCDMA_GSM
                || settingsNetworkMode == Phone.NT_MODE_LTE_TDSCDMA_WCDMA
                || settingsNetworkMode == Phone.NT_MODE_LTE_TDSCDMA_GSM_WCDMA
                || settingsNetworkMode == Phone.NT_MODE_LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA) {
            isLTE = true;
        }
        mLteSwitchPreference.setChecked(isLTE);
        mLteSwitchon = getResources().getBoolean(com.android.internal.R.bool.feature_tctfw_lteswitch_on);
        if (!mLteSwitchon) {
            PreferenceScreen preferenceScreen = this.getPreferenceScreen();
            preferenceScreen.removePreference(mLteSwitchPreference);
        }
          //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
          /* MODIFIED-END by Dandan.FANG@tcl.com,BUG-2817990*/

        //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
        mPhoneNum = TelephonyManager.getDefault().getPhoneCount();
        mAnyCard = false;
        mSimCardNum = 0;
        for (int i = 0; i < mPhoneNum; i++) {
            if(TelephonyManager.getDefault().getSimState(i) != TelephonyManager.SIM_STATE_ABSENT) {
                mAnyCard = true;
                mSimCardNum++;
            }
        }
        if((mSimCardNum!=2)&&isTurnOnBestNetwork){
            mBestNetworkModePreference.setEnabled(false);
        }
        if(!mAnyCard) {
            findPreference(KEY_MOBILE_NETWORK_SETTINGS).setEnabled(false);
            mButton4glte.setEnabled(false);
            mLteSwitchPreference.setEnabled(false);
        }
        //[FEATURE]-Add-END by TCTNB.bo.chen
        String toggleable = Settings.Global.getString(activity.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_TOGGLEABLE_RADIOS);

        //enable/disable wimax depending on the value in config.xml
        final boolean isWimaxEnabled = isAdmin && this.getResources().getBoolean(
                com.android.internal.R.bool.config_wimaxEnabled);
        if (!isWimaxEnabled || RestrictedLockUtils.hasBaseUserRestriction(activity,
                UserManager.DISALLOW_CONFIG_MOBILE_NETWORKS, UserHandle.myUserId())) {
            PreferenceScreen root = getPreferenceScreen();
            Preference ps = findPreference(KEY_WIMAX_SETTINGS);
            if (ps != null) root.removePreference(ps);
        } else {
            if (toggleable == null || !toggleable.contains(Settings.Global.RADIO_WIMAX )
                    && isWimaxEnabled) {
                Preference ps = findPreference(KEY_WIMAX_SETTINGS);
                //ps.setDependency(KEY_TOGGLE_AIRPLANE);
            }
        }

        // Manually set dependencies for Wifi when not toggleable.
        if (toggleable == null || !toggleable.contains(Settings.Global.RADIO_WIFI)) {
            //findPreference(KEY_VPN_SETTINGS).setDependency(KEY_TOGGLE_AIRPLANE);
        }
        // Disable VPN.
        // TODO: http://b/23693383
        if (!isAdmin || RestrictedLockUtils.hasBaseUserRestriction(activity,
                UserManager.DISALLOW_CONFIG_VPN, UserHandle.myUserId())) {
            removePreference(KEY_VPN_SETTINGS);
        }

        // Manually set dependencies for Bluetooth when not toggleable.
        if (toggleable == null || !toggleable.contains(Settings.Global.RADIO_BLUETOOTH)) {
            // No bluetooth-dependent items in the list. Code kept in case one is added later.
        }

        // Manually set dependencies for NFC when not toggleable.
        if (toggleable == null || !toggleable.contains(Settings.Global.RADIO_NFC)) {
            //findPreference(KEY_TOGGLE_NFC).setDependency(KEY_TOGGLE_AIRPLANE);
            //findPreference(KEY_ANDROID_BEAM_SETTINGS).setDependency(KEY_TOGGLE_AIRPLANE);
        }

        // Remove NFC if not available
        mNfcAdapter = NfcAdapter.getDefaultAdapter(activity);
        if (mNfcAdapter == null) {
            getPreferenceScreen().removePreference(nfc);
            getPreferenceScreen().removePreference(androidBeam);
            mNfcEnabler = null;
        }
        getPreferenceScreen().removePreference(nfc);
        getPreferenceScreen().removePreference(androidBeam);

        // Remove Mobile Network Settings and Manage Mobile Plan for secondary users,
        // if it's a wifi-only device.
        /* MODIFIED-BEGIN by liang.zhang1-nb, 2016-09-07,BUG-2827969*/
        /*if (!isAdmin || Utils.isWifiOnly(getActivity()) ||
                RestrictedLockUtils.hasBaseUserRestriction(activity,
                        UserManager.DISALLOW_CONFIG_MOBILE_NETWORKS, UserHandle.myUserId())) {*/
            removePreference(KEY_MOBILE_NETWORK_SETTINGS);
            removePreference(KEY_MANAGE_MOBILE_PLAN);
        /*} else {
            mIsNetworkSettingsAvailable = Utils.isNetworkSettingsApkAvailable(getActivity());
        }*/
        /* MODIFIED-END by liang.zhang1-nb,BUG-2827969*/
        // Remove Mobile Network Settings and Manage Mobile Plan
        // if config_show_mobile_plan sets false.
        final boolean isMobilePlanEnabled = this.getResources().getBoolean(
                R.bool.config_show_mobile_plan);
        if (!isMobilePlanEnabled) {
            Preference pref = findPreference(KEY_MANAGE_MOBILE_PLAN);
            if (pref != null) {
                removePreference(KEY_MANAGE_MOBILE_PLAN);
            }
        }

        // Remove Airplane Mode settings if it's a stationary device such as a TV.
        /* MODIFIED-BEGIN by liang.zhang1-nb, 2016-09-07,BUG-2827969*/
        //if (mPm.hasSystemFeature(PackageManager.FEATURE_TELEVISION)) {
            removePreference(KEY_TOGGLE_AIRPLANE);
        //}
        /* MODIFIED-END by liang.zhang1-nb,BUG-2827969*/

        // Enable Proxy selector settings if allowed.
        Preference mGlobalProxy = findPreference(KEY_PROXY_SETTINGS);
        final DevicePolicyManager mDPM = (DevicePolicyManager)
                activity.getSystemService(Context.DEVICE_POLICY_SERVICE);
        // proxy UI disabled until we have better app support
        getPreferenceScreen().removePreference(mGlobalProxy);
        mGlobalProxy.setEnabled(mDPM.getGlobalProxyAdmin() == null);

        // Disable Tethering if it's not allowed or if it's a wifi-only device
        final ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        final boolean adminDisallowedTetherConfig = RestrictedLockUtils.checkIfRestrictionEnforced(
                activity, UserManager.DISALLOW_CONFIG_TETHERING, UserHandle.myUserId()) != null;
        /* MODIFIED-BEGIN by liang.zhang1-nb, 2016-09-07,BUG-2827969*/
        if ((!cm.isTetheringSupported() && !adminDisallowedTetherConfig) ||
                RestrictedLockUtils.hasBaseUserRestriction(activity,
                        UserManager.DISALLOW_CONFIG_TETHERING, UserHandle.myUserId())) {
            getPreferenceScreen().removePreference(findPreference(KEY_TETHER_SETTINGS));
        } else if (!adminDisallowedTetherConfig) {
            Preference p = findPreference(KEY_TETHER_SETTINGS);
            p.setTitle(com.android.settingslib.Utils.getTetheringLabel(cm));

            // Grey out if provisioning is not available.
            p.setEnabled(!TetherSettings
                    .isProvisioningNeededButUnavailable(getActivity()));
        }
        /* MODIFIED-END by liang.zhang1-nb,BUG-2827969*/

        //[FEATURE]-ADD_BEGIN by TSNJ.heng.zhang1,2016-09-23,Task-2827972
        mToggleLocation = (SwitchPreference) findPreference(KEY_TOGGLE_LOCATION);
        if (mToggleLocation != null) {
            mCurrentLocationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            mToggleLocation.setChecked((mCurrentLocationMode != Settings.Secure.LOCATION_MODE_OFF) ? true : false);
            mToggleLocation.setOnPreferenceChangeListener(this);
        } else {
            removePreference(KEY_TOGGLE_LOCATION);
        }
        createLocationSettingsReceiver();
        //[FEATURE]-ADD_END by TSNJ.heng.zhang1,2016-09-23,Task-2827972
      //[FEATURE]-Mod-BEGIN by TCTNB.dongdong.gong,10/25/2016,3082231,
      //move the usb and bluetooth tether to another place.
        mDataSaverBackend = new DataSaverBackend(getContext());
        mDataSaverEnabled = mDataSaverBackend.isDataSaverEnabled();
        mDataSaverBackend.addListener(this);
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter != null) {
            adapter.getProfileProxy(activity.getApplicationContext(), mProfileServiceListener,
                    BluetoothProfile.PAN);
        }
        mUsbRegexs = mCm.getTetherableUsbRegexs();
        mBluetoothRegexs = mCm.getTetherableBluetoothRegexs();
        final boolean usbAvailable = mUsbRegexs.length != 0;
        final boolean bluetoothAvailable = mBluetoothRegexs.length != 0;
        mUsbTether = (SwitchPreference) findPreference(USB_TETHER_SETTINGS);
        mBluetoothTether = (SwitchPreference) findPreference(ENABLE_BLUETOOTH_TETHERING);
        mDataSaverFooter = findPreference(DATA_SAVER_FOOTER);
        if (!usbAvailable || Utils.isMonkeyRunning()) {
            getPreferenceScreen().removePreference(mUsbTether);
        }

        if (!bluetoothAvailable) {
            getPreferenceScreen().removePreference(mBluetoothTether);
        } else {
            BluetoothPan pan = mBluetoothPan.get();
            if (pan != null && pan.isTetheringOn()) {
                mBluetoothTether.setChecked(true);
            } else {
                mBluetoothTether.setChecked(false);
            }
        }
        // Set initial state based on Data Saver mode.
        onDataSaverChanged(mDataSaverBackend.isDataSaverEnabled());
        //[FEATURE]-Mod-END by TCTNB.dongdong.gong
    }

    //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
    @Override
    public void onStart() {
        super.onStart();
      //[FEATURE]-Mod-BEGIN by TCTNB.dongdong.gong,10/25/2016,3082231,
      //move the usb and bluetooth tether to another place.
        final Activity activity = getActivity();
        mStartTetheringCallback = new OnStartTetheringCallback(this);
        mMassStorageActive = Environment.MEDIA_SHARED.equals(Environment.getExternalStorageState());
        mReceiver = new NetworkSimStateChangedReceiver();
        IntentFilter filter = new IntentFilter(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        filter.addAction(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED);
        if (getActivity() != null) {
            getActivity().registerReceiver(mReceiver, filter);
        }
        mTetherChangeReceiver = new TetherChangeReceiver();
        filter.addAction(ConnectivityManager.ACTION_TETHER_STATE_CHANGED);
        Intent intent = activity.registerReceiver(mTetherChangeReceiver, filter);
        activity.registerReceiver(mTetherChangeReceiver, filter);
        filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_STATE);
        activity.registerReceiver(mTetherChangeReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_SHARED);
        filter.addAction(Intent.ACTION_MEDIA_UNSHARED);
        filter.addDataScheme("file");
        activity.registerReceiver(mTetherChangeReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        activity.registerReceiver(mTetherChangeReceiver, filter);
        if (intent != null) mTetherChangeReceiver.onReceive(activity, intent);
        updateState();
        //[FEATURE]-Mod-END by TCTNB.dongdong.gong
    }
    //[FEATURE]-Add-END by TCTNB.bo.chen

    @Override
    public void onResume() {
        super.onResume();

        mAirplaneModeEnabler.resume();
        if (mNfcEnabler != null) {
            mNfcEnabler.resume();
        }

        // update WFC setting
        final Context context = getActivity();
        if (ImsManager.isWfcEnabledByPlatform(context)) {
            getPreferenceScreen().addPreference(mButtonWfc);

            mButtonWfc.setSummary(WifiCallingSettings.getWfcModeSummary(
                    context, ImsManager.getWfcMode(context)));
        } else {
            removePreference(KEY_WFC_SETTINGS);
        }

        /* MODIFIED-BEGIN by Dandan.FANG@tcl.com, 2016-09-02,BUG-2817990*/
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/15/2016,TASK2721235,
        //[Telecom_Internal[GL/CN]] VoLTE contorl menu
        // Enable enhanced 4G LTE mode settings depending on whether exists on platform
        // or in multi-sim cases, depending on whether ImsPhone exists for that subscription
        if (!(ImsManager.isVolteEnabledByPlatform(context)
                && ImsManager.isVolteProvisionedOnDevice(context))) {
            removePreference(BUTTON_4G_LTE_KEY);
        }
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        /* MODIFIED-END by Dandan.FANG@tcl.com,BUG-2817990*/
        initLocationSettings();//add by heng.zhang1 ,2016-09-23 , Task-2827972
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (!TextUtils.isEmpty(mManageMobilePlanMessage)) {
            outState.putString(SAVED_MANAGE_MOBILE_PLAN_MSG, mManageMobilePlanMessage);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mAirplaneModeEnabler.pause();
        if (mNfcEnabler != null) {
            mNfcEnabler.pause();
        }
        disableLocationSettings();//add by heng.zhang1 ,2016-09-23 , Task-2827972
    }
    //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
    @Override
    public void onStop() {
        super.onStop();
        if (getActivity() != null) {
            getActivity().unregisterReceiver(mReceiver);
        }
      //[FEATURE]-Mod-BEGIN by TCTNB.dongdong.gong,10/25/2016,3082231,
      //move the usb and bluetooth tether to another place.
        getActivity().unregisterReceiver(mTetherChangeReceiver);
        mTetherChangeReceiver = null;
        mStartTetheringCallback = null;
        //[FEATURE]-Mod-END by TCTNB.dongdong.gong
    }
    //[FEATURE]-Add-END by TCTNB.bo.chen

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_EXIT_ECM) {
            Boolean isChoiceYes = data.getBooleanExtra(EXIT_ECM_RESULT, false);
            // Set Airplane mode based on the return value and checkbox state
            mAirplaneModeEnabler.setAirplaneModeInECM(isChoiceYes,
                    mAirplaneModePreference.isChecked());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected int getHelpResource() {
        return R.string.help_url_more_networks;
    }

    //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
    private class NetworkSimStateChangedReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {
                boolean isAnyCard = false;
                mSimCardNum = 0;
                for (int i = 0; i < mPhoneNum; i++) {
                    if(TelephonyManager.getDefault().getSimState(i) != TelephonyManager.SIM_STATE_ABSENT) {
                        isAnyCard = true;
                        mSimCardNum++;
                    }
                }
                if((mSimCardNum!=2)&&(getResources().getBoolean(com.android.internal.R.bool.def_tctfw_isTurnOnBestNetworkAcess))){
                    mBestNetworkModePreference.setEnabled(false);
                }
                if(mAnyCard != isAnyCard) {
                    mAnyCard = isAnyCard;
                    if (findPreference(KEY_MOBILE_NETWORK_SETTINGS) != null) {
                    findPreference(KEY_MOBILE_NETWORK_SETTINGS).setEnabled(mAnyCard);
                    }
                    if (findPreference(KEY_TOGGLE_LTE_SWITCH) != null) {
                    findPreference(KEY_TOGGLE_LTE_SWITCH).setEnabled(mAnyCard);//[FEATURE]-Add by TCTNB.bo.chen,03/09/2016,1714708,4G Switch
                    }
                    if (findPreference(BUTTON_4G_LTE_KEY) != null){
                        findPreference(BUTTON_4G_LTE_KEY).setEnabled(mAnyCard);
                    }
                }
            }
        }
    }
    //[FEATURE]-Add-END by TCTNB.bo.chen

    /**
     * For Search.
     */
    public static final Indexable.SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
        new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(
                    Context context, boolean enabled) {
                SearchIndexableResource sir = new SearchIndexableResource(context);
                sir.xmlResId = R.xml.wireless_settings;
                return Arrays.asList(sir);
            }

            @Override
            public List<String> getNonIndexableKeys(Context context) {
                final ArrayList<String> result = new ArrayList<String>();

                final UserManager um = (UserManager) context.getSystemService(Context.USER_SERVICE);
                final boolean isSecondaryUser = !um.isAdminUser();
                final boolean isWimaxEnabled = !isSecondaryUser
                        && context.getResources().getBoolean(
                        com.android.internal.R.bool.config_wimaxEnabled);
                if (!isWimaxEnabled) {
                    result.add(KEY_WIMAX_SETTINGS);
                }

                if (isSecondaryUser) { // Disable VPN
                    result.add(KEY_VPN_SETTINGS);
                }

                // Remove NFC if not available
                final NfcManager manager = (NfcManager)
                        context.getSystemService(Context.NFC_SERVICE);
                if (manager != null) {
                    NfcAdapter adapter = manager.getDefaultAdapter();
                    if (adapter == null) {
                        result.add(KEY_TOGGLE_NFC);
                        result.add(KEY_ANDROID_BEAM_SETTINGS);
                    }
                }

                // Remove Mobile Network Settings and Manage Mobile Plan if it's a wifi-only device.
                if (isSecondaryUser || Utils.isWifiOnly(context)) {
                    result.add(KEY_MOBILE_NETWORK_SETTINGS);
                    result.add(KEY_MANAGE_MOBILE_PLAN);
                }

                // Remove Mobile Network Settings and Manage Mobile Plan
                // if config_show_mobile_plan sets false.
                final boolean isMobilePlanEnabled = context.getResources().getBoolean(
                        R.bool.config_show_mobile_plan);
                if (!isMobilePlanEnabled) {
                    result.add(KEY_MANAGE_MOBILE_PLAN);
                }

                final PackageManager pm = context.getPackageManager();

                // Remove Airplane Mode settings if it's a stationary device such as a TV.
                if (pm.hasSystemFeature(PackageManager.FEATURE_TELEVISION)) {
                    result.add(KEY_TOGGLE_AIRPLANE);
                }

                // proxy UI disabled until we have better app support
                result.add(KEY_PROXY_SETTINGS);

                // Disable Tethering if it's not allowed or if it's a wifi-only device
                ConnectivityManager cm = (ConnectivityManager)
                        context.getSystemService(Context.CONNECTIVITY_SERVICE);
                /* MODIFIED-BEGIN by liang.zhang1-nb, 2016-09-07,BUG-2827969*/
                if (isSecondaryUser || !cm.isTetheringSupported()) {
                    result.add(KEY_TETHER_SETTINGS);
                }
                /* MODIFIED-END by liang.zhang1-nb,BUG-2827969*/

                if (!ImsManager.isWfcEnabledByPlatform(context)) {
                    result.add(KEY_WFC_SETTINGS);
                }

                return result;
            }
        };

      /* MODIFIED-BEGIN by Dandan.FANG@tcl.com, 2016-09-02,BUG-2817990*/
      //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/15/2016,TASK2721235,
      //[Telecom_Internal[GL/CN]] VoLTE contorl menu
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            // TODO Auto-generated method stub
            log("onPreferenceChange: preference=" + preference);
        //[FEATURE]-ADD by TCTNB.bo.chen,09/07/2016,Task-2852672
        if (preference == mBestNetworkModePreference) {
            SwitchPreference mPref = (SwitchPreference)preference;
            boolean isCheck = (Boolean) newValue;
            if (isCheck) {
                SharedPreferences sp = getActivity().getSharedPreferences(BEST_NETWORK_TOAST, Context.MODE_PRIVATE);
                if(sp.getBoolean("isHideToast", false)){
                    log("isHideToast is " + sp.getBoolean("isHideToast", false));
                    mBestNetworkModePreference.setChecked(true);
                    Settings.Global.putInt(getContentResolver(), "is_select_best_network", 1);
                }else{
                    AlertDialog dialog=(AlertDialog) onCreateDialog(BEST_ACCESS_NETWORK_DIALOG_ID);
                    dialog.show();
                }
            }else {
                mPref.setChecked(false);
                Settings.Global.putInt(getContentResolver(), "is_select_best_network", 0);
            }
            Log.d(TAG,"isChecked = "+mPref.isChecked()+" ,newValue = "+newValue);
            return true;
            //[FEATURE]-Add-END by TCTNB.bo.chen
            }else if (preference == mButton4glte) {
                SwitchPreference enhanced4gModePref = (SwitchPreference) preference;
                boolean enhanced4gMode = !enhanced4gModePref.isChecked();
                enhanced4gModePref.setChecked(enhanced4gMode);
                ImsManager.setEnhanced4gLteModeSetting(getActivity().getBaseContext(), enhanced4gModePref.isChecked());
            }
            // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,08/30/2016,TASK2817990 ,
            // [4G Switch]ADD 4G button for CTCC
        else if (preference == mLteSwitchPreference) {
            SwitchPreference mLteSwitch = (SwitchPreference) preference;
            boolean lteMode = !mLteSwitch.isChecked();
            mLteSwitch.setChecked(lteMode);
            int mSubId = SubscriptionManager.from(getActivity())
                    .getDefaultDataSubscriptionId();
            int mSlotId = SubscriptionManager.from(getActivity()).getSlotId(
                    mSubId);
            try {
                settingsNetworkMode = android.telephony.TelephonyManager
                        .getIntAtIndex(
                                getActivity().getContentResolver(),
                                android.provider.Settings.Global.PREFERRED_NETWORK_MODE,
                                mSlotId);
            } catch (SettingNotFoundException snfe) {
                settingsNetworkMode = Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
            }

            int changeMode = settingsNetworkMode;
            log("LTE Switch--  settingnetworkmode1 = " + settingsNetworkMode);
            if (lteMode == true) {
                switch (settingsNetworkMode) {
                case Phone.NT_MODE_GSM_UMTS:
                    changeMode = Phone.NT_MODE_LTE_GSM_WCDMA;
                    break;
                case Phone.NT_MODE_TDSCDMA_GSM:
                    changeMode = Phone.NT_MODE_LTE_TDSCDMA_GSM;
                    break;
                case Phone.NT_MODE_CDMA:
                    changeMode = Phone.NT_MODE_LTE_CDMA_AND_EVDO;
                    break;
                case Phone.NT_MODE_GLOBAL:
                    changeMode = Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
                    break;
                }
            } else {
                switch (settingsNetworkMode) {
                case Phone.NT_MODE_LTE_GSM_WCDMA:
                    changeMode = Phone.NT_MODE_GSM_UMTS;
                    break;
                case Phone.NT_MODE_LTE_TDSCDMA_GSM:
                    changeMode = Phone.NT_MODE_TDSCDMA_GSM;
                    break;
                case Phone.NT_MODE_LTE_CDMA_AND_EVDO:
                    changeMode = Phone.NT_MODE_CDMA;
                    break;
                case Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA:
                    changeMode = Phone.NT_MODE_GLOBAL;
                    break;
                }
            }
            if (mSlotId > -1) {
                android.telephony.TelephonyManager
                        .putIntAtIndex(
                                getActivity().getContentResolver(),
                                android.provider.Settings.Global.PREFERRED_NETWORK_MODE,
                                mSlotId, changeMode);
            }
            mTm.setPreferredNetworkType(SubscriptionManager.from(getActivity())
                    .getDefaultDataSubscriptionId(), changeMode);
            return true;
            //[FEATURE]-ADD_BEGIN by TSNJ.heng.zhang1,2016-09-23,Task-2827972
        } else if (preference == mToggleLocation) {
            boolean isChecked = (Boolean) newValue;
            if (isChecked) {
                setLocationMode(android.provider.Settings.Secure.LOCATION_MODE_HIGH_ACCURACY);
            } else {
                setLocationMode(android.provider.Settings.Secure.LOCATION_MODE_OFF);
            }
            return true;
        }
            //[FEATURE]-ADD_END by TSNJ.heng.zhang1,2016-09-23,Task-2827972
        // [BUGFIX]-Add-END by TCTNB.Dandan.Fang
            return false;
        }
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        /* MODIFIED-END by Dandan.FANG@tcl.com,BUG-2817990*/

    //[FEATURE]-ADD_BEGIN by TSNJ.heng.zhang1,2016-09-23,Task-2827972
    public void createLocationSettingsReceiver() {
        mLocationSettingsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Log.isLoggable(TAG, Log.DEBUG)) {
                    Log.d(TAG, "Received location mode change intent: " + intent);
                }
                refreshLocationMode();
            }
        };
    }

    //init in onCreate()
    public void initLocationSettings() {
        mLocatioSettingsActive = true;
        IntentFilter filter = new IntentFilter();
        filter.addAction(LocationManager.MODE_CHANGED_ACTION);
        getActivity().registerReceiver(mLocationSettingsReceiver, filter);
    }

    //disable in onPause()
    public void disableLocationSettings() {
        try {
            getActivity().unregisterReceiver(mLocationSettingsReceiver);
        } catch (RuntimeException e) {
            // Ignore exceptions caused by race condition
        }
        mLocatioSettingsActive = false;
    }

    private boolean isRestricted() {
        final UserManager um = (UserManager) getActivity().getSystemService(Context.USER_SERVICE);
        return um.hasUserRestriction(UserManager.DISALLOW_SHARE_LOCATION);
    }

    public void setLocationMode(int mode) {
        if (isRestricted()) {
            // Location toggling disabled by user restriction. Read the current location mode to
            // update the location master switch.
            if (Log.isLoggable(TAG, Log.INFO)) {
                Log.i(TAG, "Restricted user, not setting location mode");
            }
            mode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            if (mLocatioSettingsActive) {
                onModeChanged(mode, true);
            }
            return;
        }
        Intent intent = new Intent(MODE_CHANGING_ACTION);
        intent.putExtra(CURRENT_MODE_KEY, mCurrentLocationMode);
        intent.putExtra(NEW_MODE_KEY, mode);
        getActivity().sendBroadcast(intent, android.Manifest.permission.WRITE_SECURE_SETTINGS);
        Settings.Secure.putInt(getContentResolver(), Settings.Secure.LOCATION_MODE, mode);
        refreshLocationMode();
    }

    public void refreshLocationMode() {
        if (mLocatioSettingsActive) {
            int mode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            mCurrentLocationMode = mode;
            if (Log.isLoggable(TAG, Log.INFO)) {
                Log.i(TAG, "Location mode has been changed");
            }
            onModeChanged(mode, isRestricted());
        }
    }

    private void onModeChanged(int mode, boolean restricted) {
        //add your code when location mode changed
    }
    //[FEATURE]-ADD_END by TSNJ.heng.zhang1,2016-09-23,Task-2827972

  //[FEATURE]-Mod-BEGIN by TCTNB.dongdong.gong,10/25/2016,3082231,
  //move the usb and bluetooth tether to another place.
    private class TetherChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context content, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.ACTION_TETHER_STATE_CHANGED)) {
                // TODO - this should understand the interface types
                ArrayList<String> available = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_AVAILABLE_TETHER);
                ArrayList<String> active = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_ACTIVE_TETHER);
                ArrayList<String> errored = intent.getStringArrayListExtra(
                        ConnectivityManager.EXTRA_ERRORED_TETHER);
                updateState(available.toArray(new String[available.size()]),
                        active.toArray(new String[active.size()]),
                        errored.toArray(new String[errored.size()]));
            }
            else if (action.equals(Intent.ACTION_MEDIA_SHARED)) {
                mMassStorageActive = true;
                updateState();
            } else if (action.equals(Intent.ACTION_MEDIA_UNSHARED)) {
                mMassStorageActive = false;
                updateState();
            } else if (action.equals(UsbManager.ACTION_USB_STATE)) {
                mUsbConnected = intent.getBooleanExtra(UsbManager.USB_CONNECTED, false);
                updateState();
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                if (mBluetoothEnableForTether) {
                    switch (intent
                            .getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                        case BluetoothAdapter.STATE_ON:
                            startTethering(TETHERING_BLUETOOTH);
                            mBluetoothEnableForTether = false;
                            break;

                        case BluetoothAdapter.STATE_OFF:
                        case BluetoothAdapter.ERROR:
                            mBluetoothEnableForTether = false;
                            break;

                        default:
                            // ignore transition states
                    }
                }
                updateState();
            }
        }
    }

    private void updateState() {
        String[] available = mCm.getTetherableIfaces();
        String[] tethered = mCm.getTetheredIfaces();
        String[] errored = mCm.getTetheringErroredIfaces();
        updateState(available, tethered, errored);
    }

    private void updateState(String[] available, String[] tethered,
            String[] errored) {
        updateUsbState(available, tethered, errored);
        updateBluetoothState(available, tethered, errored);
    }


    private void updateUsbState(String[] available, String[] tethered,
            String[] errored) {
        boolean usbAvailable = mUsbConnected && !mMassStorageActive;
        int usbError = ConnectivityManager.TETHER_ERROR_NO_ERROR;
        for (String s : available) {
            for (String regex : mUsbRegexs) {
                if (s.matches(regex)) {
                    if (usbError == ConnectivityManager.TETHER_ERROR_NO_ERROR) {
                        usbError = mCm.getLastTetherError(s);
                    }
                }
            }
        }
        boolean usbTethered = false;
        for (String s : tethered) {
            for (String regex : mUsbRegexs) {
                if (s.matches(regex)) usbTethered = true;
            }
        }
        boolean usbErrored = false;
        for (String s: errored) {
            for (String regex : mUsbRegexs) {
                if (s.matches(regex)) usbErrored = true;
            }
        }
        if (usbTethered) {
            mUsbTether.setSummary(R.string.usb_tethering_active_subtext);
            mUsbTether.setEnabled(!mDataSaverEnabled);
            mUsbTether.setChecked(true);
        } else if (usbAvailable) {
            if (usbError == ConnectivityManager.TETHER_ERROR_NO_ERROR) {
                mUsbTether.setSummary(R.string.usb_tethering_available_subtext);
            } else {
                mUsbTether.setSummary(R.string.usb_tethering_errored_subtext);
            }
            mUsbTether.setEnabled(!mDataSaverEnabled);
            mUsbTether.setChecked(false);
        } else if (usbErrored) {
            mUsbTether.setSummary(R.string.usb_tethering_errored_subtext);
            mUsbTether.setEnabled(false);
            mUsbTether.setChecked(false);
        } else if (mMassStorageActive) {
            mUsbTether.setSummary(R.string.usb_tethering_storage_active_subtext);
            mUsbTether.setEnabled(false);
            mUsbTether.setChecked(false);
        } else {
            mUsbTether.setSummary(R.string.usb_tethering_unavailable_subtext);
            mUsbTether.setEnabled(false);
            mUsbTether.setChecked(false);
        }
    }

    private void updateBluetoothState(String[] available, String[] tethered,
            String[] errored) {
        boolean bluetoothErrored = false;
        for (String s: errored) {
            for (String regex : mBluetoothRegexs) {
                if (s.matches(regex)) bluetoothErrored = true;
            }
        }

        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {
            return;
        }
        int btState = adapter.getState();
        if (btState == BluetoothAdapter.STATE_TURNING_OFF) {
            mBluetoothTether.setEnabled(false);
            mBluetoothTether.setSummary(R.string.bluetooth_turning_off);
        } else if (btState == BluetoothAdapter.STATE_TURNING_ON) {
            mBluetoothTether.setEnabled(false);
            mBluetoothTether.setSummary(R.string.bluetooth_turning_on);
        } else {
            BluetoothPan bluetoothPan = mBluetoothPan.get();
            if (btState == BluetoothAdapter.STATE_ON && bluetoothPan != null
                    && bluetoothPan.isTetheringOn()) {
                mBluetoothTether.setChecked(true);
                mBluetoothTether.setEnabled(!mDataSaverEnabled);
                int bluetoothTethered = bluetoothPan.getConnectedDevices().size();
                if (bluetoothTethered > 1) {
                    String summary = getString(
                            R.string.bluetooth_tethering_devices_connected_subtext,
                            bluetoothTethered);
                    mBluetoothTether.setSummary(summary);
                } else if (bluetoothTethered == 1) {
                    mBluetoothTether.setSummary(
                            R.string.bluetooth_tethering_device_connected_subtext);
                } else if (bluetoothErrored) {
                    mBluetoothTether.setSummary(R.string.bluetooth_tethering_errored_subtext);
                } else {
                    mBluetoothTether.setSummary(R.string.bluetooth_tethering_available_subtext);
                }
            } else {
                mBluetoothTether.setEnabled(!mDataSaverEnabled);
                mBluetoothTether.setChecked(false);
                mBluetoothTether.setSummary(R.string.bluetooth_tethering_off_subtext);
            }
        }
    }

    private BluetoothProfile.ServiceListener mProfileServiceListener =
            new BluetoothProfile.ServiceListener() {
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            mBluetoothPan.set((BluetoothPan) proxy);
        }
        public void onServiceDisconnected(int profile) {
            mBluetoothPan.set(null);
        }
    };

    private void startTethering(int choice) {
        if (choice == TETHERING_BLUETOOTH) {
            // Turn on Bluetooth first.
            BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            if (adapter.getState() == BluetoothAdapter.STATE_OFF) {
                mBluetoothEnableForTether = true;
                adapter.enable();
                mBluetoothTether.setSummary(R.string.bluetooth_turning_on);
                mBluetoothTether.setEnabled(false);
                return;
            }
        }

        mCm.startTethering(choice, true, mStartTetheringCallback, mHandler);
    }

    private static final class OnStartTetheringCallback extends
            ConnectivityManager.OnStartTetheringCallback {
        final WeakReference<WirelessSettings> mTetherSettings;

        OnStartTetheringCallback(WirelessSettings settings) {
            mTetherSettings = new WeakReference<WirelessSettings>(settings);
        }

        @Override
        public void onTetheringStarted() {
            update();
        }

        @Override
        public void onTetheringFailed() {
            update();
        }

        private void update() {
            WirelessSettings settings = mTetherSettings.get();
            if (settings != null) {
                settings.updateState();
            }
        }
    }
    @Override
    public void onDataSaverChanged(boolean isDataSaving) {
        mDataSaverEnabled = isDataSaving;
        if(mUsbTether != null)
            mUsbTether.setEnabled(!mDataSaverEnabled);
        if(mBluetoothTether != null)
            mBluetoothTether.setEnabled(!mDataSaverEnabled);
        if(mDataSaverFooter != null)
            mDataSaverFooter.setVisible(mDataSaverEnabled);
    }

    @Override
    public void onWhitelistStatusChanged(int uid, boolean isWhitelisted) {
    }

    @Override
    public void onBlacklistStatusChanged(int uid, boolean isBlacklisted)  {
    }
    @Override
    public void onDestroy() {
        mDataSaverBackend.remListener(this);
        super.onDestroy();
    }
    //[FEATURE]-Mod-END by TCTNB.dongdong.gong
}
