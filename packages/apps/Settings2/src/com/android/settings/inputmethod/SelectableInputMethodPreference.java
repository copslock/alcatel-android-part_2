package com.android.settings.inputmethod;

import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodInfo;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.CompoundButton;
import android.widget.ImageView;
import com.android.settings.R;

/**
 * Created by ts on 9/30/16.
 */
public class SelectableInputMethodPreference extends InputMethodPreference implements View.OnClickListener{

    private static final String TAG = "SelectableInputMethod";

    SelectableInputMethodPreference(final Context context, final InputMethodInfo imi,
                                    final boolean isImeEnabler, final boolean isAllowedByOrganization,
                                    final OnSavePreferenceListener onSaveListener){
        super(context,imi,isImeEnabler,isAllowedByOrganization,onSaveListener);
        setLayoutResource(R.layout.selectable_input_method);
        setWidgetLayoutResource(R.layout.pref_widget);
    }

    private boolean mSelectable = true;
    private ImageView mImageView;
    private boolean mShow = false;
    private RadioButton mSelectIcon;
    private OnIconClickListener mIconClickListener;
    @Override
    public void onBindView(View view) {
        super.onBindView(view);
        View widgetFrame = (View) view.findViewById(com.android.internal.R.id.widget_frame);
        mSelectIcon = (RadioButton) view.findViewById(R.id.icon);
        View summaryView = view.findViewById(com.android.internal.R.id.summary);
        mImageView = (ImageView) widgetFrame.findViewById(R.id.pref_image_detail);
        if (mShow) {
            mImageView.setVisibility(View.GONE);
        }
        summaryView.setVisibility(View.VISIBLE);
        Log.d(TAG, "onBindViewHolder: getImeEnable()="+getImeEnable());
        mSelectIcon.setChecked(getImeEnable());
        mSelectIcon.setOnClickListener(this);
    }

    public void setGone(boolean visible) {
        this.mShow = visible;
        notifyChanged();
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.icon) {
            Log.d(TAG, "select icon ...");
            if (mIconClickListener != null) {
                mIconClickListener.onClick(this);
            }
        }
    }
    public View getCheckBox(){
        if (mSelectIcon != null){
            return mSelectIcon;
        }else return null;
    }
    public void setIconClickListener(OnIconClickListener l) {
        mIconClickListener = l;
    }
    public interface OnIconClickListener {
        void onClick(SelectableInputMethodPreference p);
    }

}
