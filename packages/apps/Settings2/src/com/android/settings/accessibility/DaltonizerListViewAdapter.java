/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.accessibility;


import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.RadioButton;

import com.android.settings.R;

public class DaltonizerListViewAdapter extends BaseAdapter {
    private String TAG = "DaltonizerListViewAdapter";
    private Context context;
    private List<Map<String, Object>> listItems;

    public DaltonizerListViewAdapter(Context context, List<Map<String, Object>> items) {
        this.context = context;
        this.listItems = items;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.daltonizer_mode_list_view_item, null);
            holder = new ViewHolder();
            holder.rb = (RadioButton) convertView.findViewById(R.id.button);
            holder.daltonizerModeTitleView = (TextView) convertView.findViewById(R.id.daltonizer_mode_title);
            holder.DaltonizerModeSummaryView = (TextView) convertView.findViewById(R.id.daltonizer_type_summary);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.rb.setChecked((Boolean) getListItems().get(position).get("checked"));
        holder.daltonizerModeTitleView.setText((String) getListItems().get(position).get("title"));
        holder.DaltonizerModeSummaryView.setText((String) getListItems().get(position).get("summary"));
        return convertView;
    }

    public Context getContext() {
        return context;
    }

    public List<Map<String, Object>> getListItems() {
        return listItems;
    }

    public final class ViewHolder {
        public TextView daltonizerModeTitleView;
        public TextView DaltonizerModeSummaryView;
        public RadioButton rb;
    }
}
