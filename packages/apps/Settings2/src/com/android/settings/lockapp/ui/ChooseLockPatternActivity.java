package com.android.settings.lockapp.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import mst.app.dialog.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.util.Log;

import com.android.settings.R;
import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.ui.LockPatternView.Cell;
import com.android.settings.lockapp.ui.LockPatternView.DisplayMode;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;

public class ChooseLockPatternActivity extends Activity {

    private Button goNumPassword;
    private final static String TCT_APP_LOCK = "tct_app_lock";
    //private TextView headerText;
    private String mCurrentPattern;
    protected List<LockPatternView.Cell> mChosenPattern = null;
    private Stage mUiStage = Stage.Introduction;
    protected LockPatternView mLockPatternView;
    // how long we wait to clear a wrong pattern
    private static final int WRONG_PATTERN_CLEAR_TIMEOUT_MS = 2000;
    private Boolean mShouldReturn = false;
    private Boolean isResetPW = false;
    private Boolean isFromSettings;

    private Runnable mClearPatternRunnable = new Runnable() {
        public void run() {
            mLockPatternView.clearPattern();

                if (Stage.ConfirmWrong == mUiStage) {
                    getActionBar().setTitle(ChooseLockPatternActivity.this.getResources().getString(R.string.confirm_password));
                }
        }
    };

    protected enum Stage {

        Introduction,
        
        ChoiceTooShort,

        NeedToConfirm,

        ChoiceConfirmed,

        FirstChoiceValid,

        ConfirmWrong;
    }

    protected LockPatternView.OnPatternListener mChooseNewLockPatternListener =
            new LockPatternView.OnPatternListener() {

            public void onPatternStart() {
                Log.d("yshcnm","onPatternStart");
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
                patternInProgress();
            }

            public void onPatternCleared() {
                Log.d("yshcnm","onPatternCleared");
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
            }

            public void onPatternDetected(List<LockPatternView.Cell> pattern) {
                Log.d("yshcnm","onPatternDetected");
                if (mUiStage == Stage.NeedToConfirm || mUiStage == Stage.ConfirmWrong) {
                    if (isResetPW) {//重置密码
                        Log.d("yshcnm","isResetPW +++mUiStage == Stage.NeedToConfirm || mUiStage == Stage.ConfirmWrong");
                        String patternStr = LockPatternUtilsSelf.patternToString(pattern);
                        if (patternStr != null && patternStr.equals(LockPatternUtilsSelf.loadFromPreferences(ChooseLockPatternActivity.this))) {
                            updateStage(Stage.ChoiceConfirmed);//确认失败
                        } else {
                            updateStage(Stage.ConfirmWrong);//确认成功
                        }
                    } else {
                        Log.d("yshcnm","mUiStage == Stage.NeedToConfirm || mUiStage == Stage.ConfirmWrong");
                        if (mChosenPattern == null) throw new IllegalStateException(
                                "null chosen pattern in stage 'need to confirm");
                        if (mChosenPattern.equals(pattern)) {
                            updateStage(Stage.ChoiceConfirmed);
                        } else {
                            updateStage(Stage.ConfirmWrong);
                        }
                    }
                } else if (mUiStage == Stage.Introduction || mUiStage == Stage.ChoiceTooShort){
                    if (pattern.size() < 4) {
                        Log.d("yshcnm"," updateStage(Stage.ChoiceTooShort);");
                        updateStage(Stage.ChoiceTooShort);
                    } else {
                        mChosenPattern = new ArrayList<LockPatternView.Cell>(pattern);
                        Log.d("yshcnm"," pdateStage(Stage.FirstChoiceValid);");
                        updateStage(Stage.FirstChoiceValid);
                    }
                } else {
                    throw new IllegalStateException("Unexpected stage " + mUiStage + " when "
                            + "entering the pattern.");
                }
            }

            private void patternInProgress() {
                Log.d("yshcnm","patternInProgress");
            }

            @Override
            public void onPatternCellAdded(List<Cell> pattern) {
                Log.d("yshcnm","onPatternCellAdded");
            }
     };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.activity_choose_lock_pattern);
        getActionBar().setTitle(R.string.input_password);
        goNumPassword = (Button) findViewById(R.id.num_lock);
        //headerText = (TextView) findViewById(R.id.headerText);
        mLockPatternView = (LockPatternView) findViewById(R.id.lockPattern);
        mLockPatternView.setOnPatternListener(mChooseNewLockPatternListener);
        mLockPatternView.setTactileFeedbackEnabled(true);

        isResetPW = getIntent().getBooleanExtra("reset_password", false);
        Log.d("yshcnm","isResetPW--》getBooleanExtra： "+isResetPW);
        if (isResetPW) {
            goNumPassword.setVisibility(View.GONE);
            getActionBar().setTitle(this.getResources().getString(R.string.confirm_password));//确认密码
            mUiStage = Stage.NeedToConfirm;
        }

        isFromSettings = getIntent().getBooleanExtra("from_settings", false);

        goNumPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isResetPW) {
                   // nothing to-do
                }
                else if (mUiStage == Stage.Introduction || mUiStage == Stage.ChoiceTooShort || mUiStage == Stage.FirstChoiceValid) {
                    Intent intent = new Intent(ChooseLockPatternActivity.this, ChooseLockPasswordActivity.class);//跳转到数字密码界面
                    if (isFromSettings) {
                        intent.putExtra("from_settings", true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    }
                    startActivity(intent);
                    finish();
                } else if (mUiStage == Stage.NeedToConfirm || mUiStage == Stage.ConfirmWrong) {
                    updateStage(Stage.Introduction);
                    postClearPatternRunnable();
                    goNumPassword.setText(ChooseLockPatternActivity.this.getResources().getString(R.string.number_password));
                    getActionBar().setTitle(ChooseLockPatternActivity.this.getResources().getString(R.string.input_password));
                }
            }
        });
    }

     protected void updateStage(Stage stage) {
         final Stage previousStage = mUiStage;

         mUiStage = stage;

         // the rest of the stuff varies enough that it is easier just to handle
         // on a case by case basis.
         mLockPatternView.setDisplayMode(DisplayMode.Correct);
         boolean announceAlways = false;

         switch (mUiStage) {
             case Introduction:
                 Log.d("yshcnm","Introduction");
                 mLockPatternView.clearPattern();
                 break;
             case ChoiceTooShort:
                 Log.d("yshcnm","ChoiceTooShort");
                 mLockPatternView.setDisplayMode(DisplayMode.Wrong);
                 //Toast.makeText(this, "too short", Toast.LENGTH_SHORT).show();
                 postClearPatternRunnable();
                 announceAlways = true;
                 break;
             case FirstChoiceValid:
                 Log.d("yshcnm","FirstChoiceValid");
                 updateStage(Stage.NeedToConfirm);
                 break;
             case NeedToConfirm:
                 Log.d("yshcnm","NeedToConfirm");
                 //Toast.makeText(this, "please confirm", Toast.LENGTH_SHORT).show();
                 goNumPassword.setText(this.getResources().getString(R.string.reset_password));
                 getActionBar().setTitle(this.getResources().getString(R.string.confirm_password));
                 mLockPatternView.clearPattern();
                 break;
             case ConfirmWrong:
                 Log.d("yshcnm","ConfirmWrong");
                 if (!isResetPW) {
                     getActionBar().setTitle(this.getResources().getString(R.string.password_not_compared));
                 } else {
                     Toast.makeText(this, this.getResources().getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();
                 }
                 mLockPatternView.setDisplayMode(DisplayMode.Wrong);
                 postClearPatternRunnable();
                 announceAlways = true;
                 break;
             case ChoiceConfirmed:
                 //Toast.makeText(this, "confirmed..", Toast.LENGTH_SHORT).show();
                 Log.d("yshcnm","ChoiceConfirmed");
                 if (!isResetPW) {
                     saveChosenPatternAndFinish();
                 } else {
                     mUiStage = Stage.Introduction;
                     getActionBar().setTitle(this.getResources().getString(R.string.new_password));//输入新密码
                     isResetPW = false;
                     goNumPassword.setVisibility(View.VISIBLE);
                     mLockPatternView.clearPattern();
                 }
                 break;
         }

        /* // If the stage changed, announce the header for accessibility. This
         // is a no-op when accessibility is disabled.
         if (previousStage != stage || announceAlways) {
             mHeaderText.announceForAccessibility(mHeaderText.getText());
         }*/
     }
    
     private void postClearPatternRunnable() {
         mLockPatternView.removeCallbacks(mClearPatternRunnable);
         mLockPatternView.postDelayed(mClearPatternRunnable, WRONG_PATTERN_CLEAR_TIMEOUT_MS);
     }
     
     private void saveChosenPatternAndFinish() {
         String mCurrentPattern = LockPatternUtilsSelf.patternToString(mChosenPattern);
         LockPatternUtilsSelf.saveToPreferences(this, mCurrentPattern);//存储密码
         SharedPreferenceUtil.editIsNumModel(this, false);
         Settings.System.putInt(getContentResolver(), "tct_app_lock_state", 1);

         if(isFromSettings) {//指纹 界面去关联应 用锁
             Log.d("yshcnm","isFromSettings");
             if (SharedPreferenceUtil.readIsFirst(this)) {
                 AlertDialog.Builder alert = new AlertDialog.Builder(
                         this);
                 alert.setMessage(R.string.password_dialog_message)
                         .setPositiveButton(
                                 R.string.password_dialog_set,
                                 new DialogInterface.OnClickListener() {
                                     public void onClick(
                                             DialogInterface dialog,
                                             int which) {
                                         Intent intent = new Intent(ChooseLockPatternActivity.this, ApplicationLockSaveGuardActivity.class);
                                         intent.putExtra("from_settings", true);
                                         intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                                         startActivity(intent);
                                         Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                         ChooseLockPatternActivity.this.finish();
                                     }
                                 })
                         .setNegativeButton(
                                 R.string.password_dialog_setlater,
                                 new DialogInterface.OnClickListener() {
                                     public void onClick(
                                             DialogInterface dialog,
                                             int which) {
                                         setResult(Activity.RESULT_OK);
                                         Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                         SharedPreferenceUtil.editIsFirst(ChooseLockPatternActivity.this, false);
                                         ChooseLockPatternActivity.this.finish();
                                     }
                                 });
                 //add by NJTS zhe.xu for defect 1936414 2016-04-14 begin
                 alert.setCancelable(false);
                 //add by NJTS zhe.xu for defect 1936414 2016-04-14 end
                 AlertDialog mDialog = alert.create();
                 mDialog.show();
             } else {
                 setResult(Activity.RESULT_OK);
                 Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                 SharedPreferenceUtil.editIsFirst(ChooseLockPatternActivity.this, false);
                 finish();
             }
         }
         else if (SharedPreferenceUtil.readIsFirst(this)) {//第一次进入，未设置密码
             Log.d("yshcnm","SharedPreferenceUtil.readIsFirst(this)");
             AlertDialog.Builder alert = new AlertDialog.Builder(
                     this);
             alert.setMessage(R.string.password_dialog_message)//如果密码遗忘，可以通过密码保护问题找回密码，是否现在设置
                     .setPositiveButton(
                             R.string.password_dialog_set,
                             new DialogInterface.OnClickListener() {
                                 public void onClick(
                                         DialogInterface dialog,
                                         int which) {
                                     Intent intent = new Intent(ChooseLockPatternActivity.this, ApplicationLockSaveGuardActivity.class);
                                     startActivity(intent);
                                     Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                     ChooseLockPatternActivity.this.finish();
                                 }
                             })
                     .setNegativeButton(
                             R.string.password_dialog_setlater,
                             new DialogInterface.OnClickListener() {
                                 public void onClick(
                                         DialogInterface dialog,
                                         int which) {
                                       //新的需求里，这部分移动到下层preference
                                     Intent intent = new Intent(ChooseLockPatternActivity.this, ApplicationLockActivity.class);
                                     startActivity(intent);
                                     Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                     SharedPreferenceUtil.editIsFirst(ChooseLockPatternActivity.this, false);
                                     ChooseLockPatternActivity.this.finish();
                                 }
                             });
             //add by NJTS zhe.xu for defect 1936414 2016-04-14 begin
             alert.setCancelable(false);
             //add by NJTS zhe.xu for defect 1936414 2016-04-14 end
             AlertDialog mDialog = alert.create();
             mDialog.show();
         } else {
             Log.d("yshcnm"," finish();");
             finish();
         }
     }
     
     public boolean onKeyDown(int keyCode, KeyEvent event) {
         if (keyCode == KeyEvent.KEYCODE_MENU && mUiStage == Stage.Introduction) {
             return true;
         }
         return super.onKeyDown(keyCode, event);
     }
}
