package com.android.settings.lockapp.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.System;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.lockapp.ui.ToggleSwitch;
import com.android.settings.R;

public class ApplicationLockSettingActivity extends Activity implements
        View.OnClickListener {
    private static String TCT_APP_LOCK = "tct_app_lock";
    private ToggleSwitch mToggleSwitch;
    private RelativeLayout updatePassword;
    private RelativeLayout updateQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.application_lock_setting);
        getActionBar().setTitle(R.string.interrept_settings);
        mToggleSwitch = (ToggleSwitch) findViewById(R.id.switch_widget1);
        updatePassword = (RelativeLayout) findViewById(R.id.application_update_password);
        updateQuestion = (RelativeLayout) findViewById(R.id.application_update_password_question);
        updatePassword.setOnClickListener(this);
        updateQuestion.setOnClickListener(this);
        int brightnessMode = Settings.System.getInt(getContentResolver(),
                TCT_APP_LOCK, 0);
        mToggleSwitch.setCheckedInternal(brightnessMode != 0);
        mToggleSwitch.setOnBeforeCheckedChangeListener(new ToggleSwitch.OnBeforeCheckedChangeListener() {
            @Override
            public boolean onBeforeCheckedChanged(ToggleSwitch toggleSwitch,
                    boolean checked) {
                Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, checked ? 1 : 0);
                return false;
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.application_update_password: {
                /*Intent intent =
                        new Intent(ApplicationLockSettingActivity.this, ChooseLockPatternActivity.class);
                startActivity(intent);*/

                boolean isNum = SharedPreferenceUtil
                        .readIsNumModel(ApplicationLockSettingActivity.this);

                Intent intent = new Intent();
                    if (isNum) {
                        intent.setClass(ApplicationLockSettingActivity.this,
                                ChooseLockPasswordActivity.class);

                    } else {
                        intent.setClass(ApplicationLockSettingActivity.this,
                                ChooseLockPatternActivity.class);

                    }
                    intent.putExtra("reset_password", true);
                    startActivity(intent);
                break;
            }
            case R.id.application_update_password_question: {
                Intent intent =
                        new Intent(ApplicationLockSettingActivity.this, ApplicationLockSaveGuardActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
