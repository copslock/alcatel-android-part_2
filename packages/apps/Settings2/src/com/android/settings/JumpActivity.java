package com.android.settings;

import android.app.Activity;
import android.os.Bundle;
import android.os.UserHandle;
import android.content.pm.PackageManager;
import android.content.Context;
import android.util.Log;
import android.content.Intent;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;

import java.util.List;

import com.android.settings.R;

import android.view.KeyEvent;
import android.hardware.fingerprint.FingerprintManager;

public class JumpActivity extends Activity {
    private static final int CONFIRM_REQUEST = 102;
    private static final String TAG = "JumpActivity";
    private int mUserId;
    private PackageManager localPackageManager;
    private String mPackageName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "JumpActivity onCreate");
        localPackageManager = getPackageManager();
        mUserId = getIntent().getIntExtra(
                Intent.EXTRA_USER_ID, UserHandle.myUserId());
        Log.d(TAG, "JumpActivity onCreate mUserid: " + mUserId);
        ChooseLockSettingsHelper helper =
                new ChooseLockSettingsHelper(this);
        mPackageName = getIntent().getStringExtra("package_name");
        FingerprintManager mFingerprintManager = (FingerprintManager) this.getSystemService(
                Context.FINGERPRINT_SERVICE);

        if (mFingerprintManager.hasEnrolledFingerprints()) {//finger
            Intent intent = new Intent();
            intent.setAction("com.tct.securitycenter.FingerprintVerify");
            startActivityForResult(intent, CONFIRM_REQUEST);
        } else {
            helper.launchConfirmationActivity(CONFIRM_REQUEST, getString(R.string.unlock_set_unlock_launch_picker_title), false, mUserId);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CONFIRM_REQUEST && resultCode == RESULT_OK) {
            Log.d(TAG, "RESULT_OK");
            localPackageManager.setApplicationLockStatus(mPackageName, 2);
            ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningTaskInfo> mrunningTaskInfos = activityManager.getRunningTasks(2);
            if (mrunningTaskInfos != null && mrunningTaskInfos.size() > 1) {
                String tepm = mrunningTaskInfos.get(1).topActivity.getPackageName();
                if (mPackageName != null && (!mPackageName.equals(tepm))) {
                    Intent i = localPackageManager.getLaunchIntentForPackage(mPackageName);
                    this.startActivity(i);
                    finish();
                }
            }
            finish();
        } else if (requestCode == CONFIRM_REQUEST && resultCode != RESULT_OK)

        {
            goHome(this);
        } else

        {
            finish();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                goHome(this);
                break;
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void goHome(Activity activity) {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        activity.startActivity(homeIntent);
        activity.finish();
    }
}
