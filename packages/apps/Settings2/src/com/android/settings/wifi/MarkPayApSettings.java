/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/10/11|     jianhong.yang    |     task 3046476     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings.wifi;

import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.UserManager;
import android.provider.Settings;
import mst.preference.Preference;
import mst.preference.CheckBoxPreference;
import mst.preference.PreferenceScreen;
import android.view.View;
import android.widget.Toast;
import android.util.Log;

import com.android.settings.RestrictedSettingsFragment;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settingslib.wifi.AccessPoint;

public class MarkPayApSettings extends RestrictedSettingsFragment{

    public static final String TAG = "MarkPayApSettings";
    private Context mContext;

    private MarkPayApManager mMarkPayApManager;
    private String mLastConnectApId = null;
    private String mLastConnectApSsid = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String action = intent.getAction();
            if(WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {
                WifiInfo recWifiInfo = (WifiInfo)intent.getExtra(WifiManager.EXTRA_WIFI_INFO);
                setConnectedAp(recWifiInfo);
            }
        }
    };

    public MarkPayApSettings(String restrictionKey) {
        super(restrictionKey);
        // TODO Auto-generated constructor stub
    }

     public MarkPayApSettings() {
         super(UserManager.DISALLOW_CONFIG_WIFI);
     }

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return MetricsEvent.WIFI_ADVANCED;
    }

    @Override
    public void onCreate(Bundle icicle) {
        // TODO Auto-generated method stub
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.mark_pay_ap_settings);
        initView();
        addNotice();
    }

    private void initView() {
        // TODO Auto-generated method stub
        mContext = getContext();
        mMarkPayApManager = new MarkPayApManager(mContext);

        int isSetupWizadRun = Settings.Global.getInt(getContentResolver(), Settings.Global.DEVICE_PROVISIONED, 0);
        if (isSetupWizadRun == 0) {
            View decorView = getActivity().getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |View.SYSTEM_UI_FLAG_IMMERSIVE
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(uiOptions);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mContext.registerReceiver(mReceiver, filter);

        Map<String,?> map = mMarkPayApManager.getMarkedPayApList();

        for(Map.Entry<String, ?> entry : map.entrySet()) {
            CheckBoxPreference ap = new CheckBoxPreference(mContext);
            ap.setTitle((String)entry.getValue());
            ap.setKey(entry.getKey());
            ap.setChecked(true);
            getPreferenceScreen().addPreference(ap);
        }
    }

    private void addNotice() {
        Preference notice = new Preference(mContext);
        notice.setLayoutResource(R.layout.mark_pay_ap_notice);
        notice.setSelectable(false);

        getPreferenceScreen().addPreference(notice);
    }

    private void setConnectedAp(WifiInfo wifiInfo) {
        if(wifiInfo != null) {
            if(isInfoExist(wifiInfo)) {
                CheckBoxPreference connectedAP = (CheckBoxPreference)findPreference(wifiInfo.getNetworkId() + "");
                if(connectedAP != null) {
                    String str = getResources().getString(R.string.current_connecting);
                    mLastConnectApId = wifiInfo.getNetworkId() + "";
                    mLastConnectApSsid = AccessPoint.removeDoubleQuotes(wifiInfo.getSSID());
                    connectedAP.setOrder(0);
                    connectedAP.setTitle(String.format("%s " + str,mLastConnectApSsid));
                }
            }
        } else {
            if(mLastConnectApId != null && mLastConnectApSsid != null) {
                CheckBoxPreference disConnectedAP = (CheckBoxPreference)findPreference(mLastConnectApId);
                if(disConnectedAP != null) {
                    String str = getResources().getString(R.string.current_connecting);
                    disConnectedAP.setTitle(mLastConnectApSsid);
                    disConnectedAP.setOrder(1);
                    mLastConnectApId = null;
                    mLastConnectApSsid = null;
                }
            }
        }
    }

    private boolean isInfoExist(WifiInfo wifiInfo) {
        if(mMarkPayApManager == null) {
            mMarkPayApManager = new MarkPayApManager(getContext());
        }

        Map<String,?> map = mMarkPayApManager.getMarkedPayApList();

        for(Map.Entry<String, ?> entry : map.entrySet()) {
            if(entry.getKey().equals(wifiInfo.getNetworkId() + "") &&
                    entry.getValue().equals(AccessPoint.removeDoubleQuotes(wifiInfo.getSSID()))) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,Preference preference) {
        // TODO Auto-generated method stub
        String key = preference.getKey();

        if(!((CheckBoxPreference) preference).isChecked()) {
            try{
                mMarkPayApManager.remove(Integer.parseInt(key));
            } catch (Exception e) {
                Toast.makeText(mContext, R.string.remove_pay_ap_failed, Toast.LENGTH_SHORT).show();
                return false;
            }

            getPreferenceScreen().removePreference(preference);
        }
        return super.onPreferenceTreeClick(preferenceScreen,preference);
    }
}
