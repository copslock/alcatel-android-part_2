/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import mst.app.dialog.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent; // MODIFIED by wanshun.ni, 2016-10-10,BUG-2827925
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class AlarmContactsAdapter extends BaseAdapter{
    private List<ContactsContent> persons;
    Context context;
    public AlarmContactsAdapter(Context context,List<ContactsContent> persons){
        this.context = context;
        this.persons = persons;
    }
    @Override
    public int getCount() {
        return (persons == null)?0:persons.size();
    }

    @Override
    public Object getItem(int position) {
        return persons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder{
        TextView mtv;
        TextView mnum; // MODIFIED by wanshun.ni, 2016-10-10,BUG-2827925
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ContactsContent person = (ContactsContent) getItem(position);
        ViewHolder viewHolder = new ViewHolder();
        TextView tv;
        ImageButton ib;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.alarm_contacts_item,null);
            viewHolder.mtv = (TextView) convertView.findViewById(R.id.tv);
            /* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
            viewHolder.mnum = (TextView) convertView.findViewById(R.id.con_num);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.mtv.setText(person.title);
        viewHolder.mnum.setText(person.summary);
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/

       return convertView;
    }
}
