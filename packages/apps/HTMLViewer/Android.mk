LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PACKAGE_NAME := HTMLViewer
LOCAL_STATIC_JAVA_LIBRARIES := htmljuniversalchardet

include $(BUILD_PACKAGE)
include $(CLEAR_VARS)

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := htmljuniversalchardet:libs/htmljuniversalchardet.jar
include $(BUILD_MULTI_PREBUILT)
