package com.android.providers.media;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.drm.DrmManagerClient;
import android.net.Uri;
import android.os.Message;
import android.util.TctLog;

import com.tct.drm.TctDrmManagerClient;
import com.tct.drm.TctDrmStore;

public class DrmTracker extends BroadcastReceiver {
    private static final String TAG = DrmUtil.TAG;
    //[BUGFIX]Add-BEGIN by TCTNB.Rongdi.Wang 2016/1/25 PR 1487774
    private static final String EXTERNAL_PATH = "/storage/emulated/0";
    //[BUGFIX]Add-END by TCTNB.Rongdi.Wang 2016/1/25
    @Override
    public void onReceive(Context context, Intent intent) {
        String action;
        boolean isLockScreen = false;
        if (!TctDrmManagerClient.isDrmEnabled()) {
            return ;
        }
        DrmTrackerAdapter trackerAdapter = DrmTrackerAdapter.getInstance(context);
        isLockScreen = intent.getBooleanExtra("isDrmLock", false);
        action = intent.getAction().toString();
        TctLog.d(TAG, "receive alarm broadcast action: " + action);
        Message msg = null;
        if (action.equals(TctDrmStore.ACITON_DRM_WALLPAPER_SET)) {
            String startDrmPath = null;
            startDrmPath = intent.getStringExtra("drmpath");
            TctLog.d(TAG, "Wall-paper 1: " + startDrmPath);
            startDrmPath = DrmUtil.formatPath(startDrmPath);
            TctLog.d(TAG, "Wall-paper 2: " + startDrmPath);
            if (isLockScreen) {
                DrmUtil.logd("isLockPaper");
                trackerAdapter.sendDrmLockPaperCheck(startDrmPath);
            } else {
                trackerAdapter.sendDrmWallpaerCheck( startDrmPath);
            }
        } else if (action.equals(TctDrmStore.ACITON_DRM_RINGTONE_SET)) {
            String ringType = intent.getStringExtra("ringtype");
            String uriString = null;
            uriString = intent.getStringExtra(ringType);
            TctLog.d(TAG, "|ringType:" +ringType+"|uriString:" +uriString );
            String path =  DrmUtil.getPathFromUri(context, uriString);
            trackerAdapter.sendDrmRingCheck( ringType, uriString, path);
        } else if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            /** need check the media db is ready? if not just send the delay message*/
            //trackerAdapter.sendMessageDelayed(DrmTrackerAdapter.MSG_PHONE_REBOOT,0);
            //trackerAdapter.sendMessageDelayed(DrmTrackerAdapter.MSG_PHONE_REBOOT, 500);
        } else if (action.equals(Intent.ACTION_MEDIA_UNMOUNTED) || action.equals(Intent.ACTION_MEDIA_SHARED)) {
            TctLog.d(TAG, "ACTION_MEDIA_UNMOUNTED: ");
            if (trackerAdapter.hasShutDown()) {
                return;
            }
            msg = trackerAdapter.obtainMessage(DrmTrackerAdapter.MSG_MEDIA_UNMOUNT);
            trackerAdapter.sendMessage(msg);
        } else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
            trackerAdapter.mounted();
        } else if (action.equals(Intent.ACTION_MEDIA_SCANNER_FINISHED)) {
            Uri uri = intent.getData();
            DrmUtil.logd("Meida Scanner End uri:" + uri);
            if (uri != null) {
                String scheme = uri.getScheme();
                if (ContentResolver.SCHEME_FILE.equals(scheme)) {
                    String path = DrmUtil.formatPath(uri.getPath());
                    if (path != null &&EXTERNAL_PATH.equals(path)) {
                        DrmUtil.logd("MediaScanner ---ready for media!");
                        trackerAdapter.sendMessageDelayed(DrmTrackerAdapter.MSG_PHONE_REBOOT, 0);
                    }
                }
            }
        } else if (action.equals(TctDrmStore.ACITON_DRM_MEDIA_HAS_EXPIRE_TIME)) {
            //msg = trackerAdapter.obtainMessage();
            //trackerAdapter.sendMessage(msg);
        } else {
            trackerAdapter.shutDown();
        }
    }
}
