package com.android.providers.media;

import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

class SmartContainerWrapper {
    private static String TAG = "SmartContainerWrapper";

    public static boolean WITH_OUT_ALL;
    public static boolean WITH_OUT_VIRTUAL_BOX;
    public static boolean WITH_OUT_CROSS_BOX_SHARE;
    public static boolean WITH_OUT_APP_CLONE;
    public static int CI_FLAG_VIRTUAL_BOX;

    private static Object manager;
    private static Class<?> managerClass;

    static {
        init();
    }

    public static void init() {
        WITH_OUT_ALL = getConfig("WITH_OUT_ALL", true);
        WITH_OUT_VIRTUAL_BOX = getConfig("WITH_OUT_VIRTUAL_BOX", true);
        WITH_OUT_APP_CLONE = getConfig("WITH_OUT_APP_CLONE", true);
        WITH_OUT_CROSS_BOX_SHARE = getConfig("WITH_OUT_CROSS_BOX_SHARE", true);
        CI_FLAG_VIRTUAL_BOX = getIntField("com.cmx.cmplus.ContainerInfo", "CI_FLAG_VIRTUAL_BOX", 0);

        try {
            manager = invoke("com.cmx.cmplus.SmartContainerManagerNative", "get", null);
            managerClass = Class.forName("com.cmx.cmplus.ISmartContainerManager");
        } catch (Exception e) {
            Log.e(TAG, "Failed to get manager class\n" + e);
            e.printStackTrace();
        }

        Log.d(TAG, "WITH_OUT_ALL " + WITH_OUT_ALL);
        Log.d(TAG, "WITH_OUT_VIRTUAL_BOX " + WITH_OUT_VIRTUAL_BOX);
        Log.d(TAG, "WITH_OUT_APP_CLONE " + WITH_OUT_APP_CLONE);
        Log.d(TAG, "WITH_OUT_CROSS_BOX_SHARE " + WITH_OUT_CROSS_BOX_SHARE);
        Log.d(TAG, "CI_FLAG_VIRTUAL_BOX " + CI_FLAG_VIRTUAL_BOX);
    }

    static private Object getField(String className, String fieldName) {
        try {
            Class<?> configClass = Class.forName(className);
            Field field = configClass.getField(fieldName);
            field.setAccessible(true);
            return field.get(null);
        } catch (Exception e) {
            Log.e(TAG, "Failed to get " + className + "." + fieldName + "\n" + e);
            e.printStackTrace();
        }
        return null;
    }

    static private boolean getConfig(String name, boolean def_val) {
        Object config = getField("com.cmx.cmplus.SmartContainerConfig", name);
        if (config != null)
            return (Boolean) config;
        return def_val;
    }

    static private int getIntField(String className, String fieldName, int def_val) {
        Object field = getField(className, fieldName);
        if (field != null)
            return (Integer) field;
        return def_val;
    }

    static private Method getMethod(Class<?> clazz, String methodName, Object... args) {
        try {
            Method method;
            if (args.length == 0)
                method = clazz.getMethod(methodName);
            else {
                Class<?> []param = new Class<?>[args.length];
                for (int i = 0; i < args.length; i++) {
                    param[i] = args[i].getClass();
                }
                method = clazz.getMethod(methodName, param);
            }
            return method;
        } catch (Exception e) {
            Log.e(TAG, "Failed to get method " + clazz.getName() + "." + methodName + "()\n" + e);
            e.printStackTrace();
        }

        return null;
    }

    static private Object invoke(String className, String methodName, Object obj, Object... args) {
        try {
            Class<?> clazz = Class.forName(className);
            Method method = getMethod(clazz, methodName, args);
            return method.invoke(obj, args);
        } catch (Exception e) {
            Log.e(TAG, "Failed to invoke " + className + "." + methodName + "()\n" + e);
            e.printStackTrace();
        }

        return null;
    }

    static public Object invoke(String methodName, Object... args) {
        try {
            Method method = getMethod(managerClass, methodName, args);
            return method.invoke(manager, args);
        } catch (Exception e) {
            Log.e(TAG, "Failed to invoke manager " + methodName + "()\n" + e);
            e.printStackTrace();
        }

        return null;
    }
}
