LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_PRIVILEGED_MODULE := true

LOCAL_SRC_FILES := $(call all-java-files-under,src)
LOCAL_SRC_FILES += $(call all-java-files-under, tct_src)

LOCAL_PACKAGE_NAME := TelephonyProvider
LOCAL_CERTIFICATE := platform


LOCAL_STATIC_JAVA_LIBRARIES += rcs-provider
LOCAL_JAVA_LIBRARIES += telephony-common
LOCAL_STATIC_JAVA_LIBRARIES += android-common

include $(BUILD_PACKAGE)

include $(CLEAR_VARS)
#Need change back later
#LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES += rcs-provider:./libs/rcs-provider.jack
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES += rcs-provider:../../../vendor/tctalone/TctAppPackage/RcsTelephonyProvider/rcs-provider.jack
include $(BUILD_MULTI_PREBUILT)
include $(call all-makefiles-under,$(LOCAL_PATH))
