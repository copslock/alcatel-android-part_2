/**
 * Class/ODDefines
 * Class/OMA Download
 * Copyright @ 2011 TCT, Inc. All Rights Reserved
 * Revision History:
 * Modification Tracking
 *      Author         Date      Version
 * --------------- ------------ --------- ------------------------------------
 *   yanli.zhang   2012-05-08     0.1
 *   zonghui.li    2012-11-07     add this file for drm
 */


package com.android.providers.downloads.ui.omadownloads;


/**
 * Class ODDefines
 */
public class ODDefines {
//  public static final String DOWNLOAD_SAVE_PATH = "/sdcard/download";
    public static final int MEDIA_INFORMATION_DIALOG_ID = 1;
    public static final int DOWNLOAD_SUCCESS_DIALOG_ID = 2;
    public static final int DOWNLOAD_FAILED_DIALOG_ID = 3;
    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan),12/05/2013,PR-560722,
    public static final int DOWNLOAD_UNSUPPORT_DIALOG_ID = 4;
    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
    // [BUGFIX]-MOD-BEGIN by TSNJ.(chao.yuan), 12/26/2013,PR-578536,
    public static final int DOWNLOAD_UNSUPPORT_CONFIRM_ID = 5;
    // [BUGDIX]-MOD-END by TSNJ.(chao.yuan),
    public static final String DOWNLOAD_RESULT_REPORT_NAME = "post";
    public static final String DD_FILEPATH_IDENTIFIER = "filename";
    public static final String SD_CARD_AVAILABLE_SIZE = "sdcard_available_size";
    public static final String DD_ATTRIBUTE_MEDIA = "media";
    public static final String DD_ATTRIBUTE_OBJECTURL = "objectURI";
    public static final String DD_ATTRIBUTE_SIZE = "size";
    public static final String DD_ATTRIBUTE_TYPE = "type";
    public static final String DD_ATTRIBUTE_NAME = "name";
    public static final String DD_ATTRIBUTE_VENDOR = "vendor";
    public static final String DD_ATTRIBUTE_NEXTURL = "nextURL";
    public static final String DD_ATTRIBUTE_INFOURL = "infoURL";
    public static final String DD_ATTRIBUTE_ICONURL = "iconURI";
    public static final String DD_ATTRIBUTE_INSTALLPARAM = "installParam";
    public static final String DD_ATTRIBUTE_DESCRIPTION = "description";
    public static final String DD_ATTRIBUTE_INSTALLNOTIFYURI = "installNotifyURI";
    public static final String DD_ATTRIBUTE_DDVERSION = "DDVersion";
    public static final String DD_INSTALLNOTIFYURI_ATTRIBUTE_START_FLAG = "<installNotifyURI>";
    public static final String DD_INSTALLNOTIFYURI_ATTRIBUTE_END_FLAG = "</installNotifyURI>";
    public static final String INSTALLNOTIFY_REPORT_INVALID_DESCRIPTOR = "906 Invalid Descriptor";
    public static final String INSTALLNOTIFY_REPORT_INVALID_DDVERSION = "951 Invalid DDVersion";
    public static final String INSTALLNOTIFY_REPORT_LOADER_ERROR = "954 Loder Error";
    public static final String INSTALLNOTIFY_REPORT_DOWNLOAD_SUCCESS = "900 Success";
    public static final String INSTALLNOTIFY_RESPONSE_DOWNLOAD_SUCCESS_CODE = "200";
    public static final String INSTALLNOTIFY_REPORT_USER_CANCELLED = "902 User Cancelled";
    public static final String INSTALLNOTIFY_REPORT_INSUFFICIENT_MEMORY = "901 Insufficient memory";
    public static final String INSTALLNOTIFY_REPORT_LOSS_OF_SERVICE = "903 Loss of Service";
    public static final String INSTALLNOTIFY_REPORT_ATTRIBUTE_MISMATCH = "905 Attribute mismatch";
    public static final String INSTALLNOTIFY_REPORT_DEVICE_ABORTED = "952 Device Aborted";
    public static final String INSTALLNOTIFY_REPORT_DEVICE_ABORTED_CODE = "952";
    public static final String INSTALLNOTIFY_REPORT_ACCEPTABLE_CONTENT = "953 Non-Acceptable Content";
//[BUGFIX]-Add-BEGIN by TCTNB.Rui.Liu,11/20/2013,PR556224,
    public static final String VALID_URL_REGULAR_EXPRESSION = "^((https|http|ftp|rtsp|mms)?://)"
    +"?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?"//ftp's user name
    +"(([0-9]{1,3}\\.){3}[0-9]{1,3}"//IP directory like 192.168.1.1
    + "|"// allow IP and domain
    +"([0-9a-z_!~*'()-]+\\.)*" //domain---www.
    +"([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\\."//sub-domain
    +"[a-z]{2,6})"// first level domain- .com or .museum
    +"(:[0-9]{1,4})?"//Port ---80
    +"((/?)|"//a slash isn't requiredif there is no file name
    +"(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
//[BUGFIX]-Add-END by TCTNB.Rui.Liu
}
