/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiushou.Liu                                                      */
/*  Email  :  Qiushou.Liu@tcl-mobile.com                                       */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/04/2014|qiushou.liu           |     FR736884         |[HOMO][Orange][28]*/
/*           |                      |                      | 11 - DOWNLOAD_01 */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.providers.downloads.ui.omadownloads;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

import android.os.SystemProperties;
import android.util.TctLog;
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,538864,
//[HOMO][Orange][28_1] 11 - DOWNLOAD_01 - User Agent
import com.android.providers.downloads.Constants;
//[FEATURE]-Mod-END by TCTNB.xihe.lu
//[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
import android.content.Context;
import android.os.Build;
import java.util.Locale;
import com.android.providers.downloads.ui.R;
//[FEATURE]-Mod-END by TCTNB.guoju.yao
/**
 * Class ODStatusReportor
 */
public class ODStatusReportor extends Thread {
    private String mStrResult;

//[BUGFIX]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2014, PR-772727
//[DownloadProvider][DownloadManager] User Agent is not the same as Browser.
    private Context mContext;

    public ODStatusReportor(Context context) {
        mContext = context;
    }


    public ODStatusReportor(String uri, String code,Context context) {
        mContext = context;
        this.uri = uri;
        this.code = code;
    }

    private String constructTctUserAgent(String userAgent) {
        try {
            userAgent = userAgent.replaceAll("BUILD_MODEL", Build.MODEL);
            userAgent = userAgent.replaceAll("BUILD_VERSION", Build.VERSION.RELEASE);
            userAgent = userAgent.replaceAll("BUILD_ID", Build.ID);
            userAgent = userAgent.replaceAll("LANGUAGE", Locale.getDefault().getLanguage());
            //[BUGFIX]-Mod by TCTNB.wei.guo,09/15/2014,PR772727,
            //[DownloadManager] User Agent is not the same as Browser
            userAgent = userAgent.replaceAll("country", Locale.getDefault().getCountry().toLowerCase());
            return userAgent;
        } catch (Exception ex) {
            return null;
        }
    }
//[BUGFIX]-Add-END by TCTNB.(JiangLong Pan)

    @Override
    public void run() {
        sendAndRecevie();
    }

    public String sendAndRecevie(File file, String uri, String code) {
        /**
         * It is used to add a status code to post
         */
        HttpPost httpRequest = new HttpPost(uri);

        try {
            httpRequest.setEntity(new StringEntity(code));

//[BUGFIX]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2014, PR-772727
//[DownloadProvider][DownloadManager] User Agent is not the same as Browser.
            String downloadUA = "";
            if (mContext != null) {
                //downloadUA = mContext.getResources().getString(R.string.def_download_useragent);
                downloadUA = "test";
            }
            TctLog.i("TCTLOG", "sendAndRecevie downloadUA2 = " + downloadUA);

            if("1".equals(SystemProperties.get("ro.download.useragent.enable","0"))) { // default on
                httpRequest.addHeader("User-Agent", Constants.DEFAULT_USER_AGENT);
            } else if (downloadUA != null && !downloadUA.equals("")) {
                httpRequest.addHeader("User-Agent", constructTctUserAgent(downloadUA));
                TctLog.i("TCTLOG", "sendAndRecevie constructTctUserAgent2 = " + constructTctUserAgent(downloadUA));
            }
//[BUGFIX]-Add-END by TCTNB.(JiangLong Pan)

            DefaultHttpClient httpClient = new DefaultHttpClient();
            // setting timeout
            httpClient.getParams().setIntParameter(HttpConnectionParams.SO_TIMEOUT, 3000);
            // connection timeout
            httpClient.getParams().setIntParameter(HttpConnectionParams.CONNECTION_TIMEOUT, 3000);

            HttpResponse httpResponse = httpClient.execute(httpRequest);

            // install success
            if (ODDefines.INSTALLNOTIFY_REPORT_DOWNLOAD_SUCCESS.equals(code)) {
                int status = httpResponse.getStatusLine().getStatusCode();

                // success
                if (status == 200) {
                    mStrResult = httpResponse.getStatusLine().toString();
                    TctLog.d("RequestAndResponseStatus", "ywp-strResult: "
                            + httpResponse.getStatusLine().toString());
                } else {
                    mStrResult = httpResponse.getStatusLine().toString();

                    if (file != null) {
                        file.delete();
                    }// end if
                    //[FEATURE]-Mod-BEGIN by TCTNB.guoju.yao,06/18/2014,704961
                    new ODStatusReportor(mContext).sendAndRecevie(file, uri,
                            ODDefines.INSTALLNOTIFY_REPORT_DEVICE_ABORTED);
                    //[FEATURE]-Mod-END by TCTNB.guoju.yao
                    // new ODStatusReportor().sendAndRecevie(uri,
                    // ODDefines.INSTALLNOTIFY_REPORT_DEVICE_ABORTED);
                    TctLog.d("RequestAndResponseStatus", "ywp-Error Response"
                            + httpResponse.getStatusLine().toString());
                }// end if
            }// end if
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }// end try

        return mStrResult;
    }

    public String sendAndRecevie() {
        /**
         * It is used to add a status code to post
         */
        HttpPost httpRequest = new HttpPost(uri);

        try {
            httpRequest.setEntity(new StringEntity(code));
//[FEATURE]-Mod-BEGIN by TCTNB.xihe.lu,10/23/2013,538864,
//[HOMO][Orange][28_1] 11 - DOWNLOAD_01 - User Agent
//[BUGFIX]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2014, PR-772727
//[DownloadProvider][DownloadManager] User Agent is not the same as Browser.
            String downloadUA = "";
            if (mContext != null) {
                // fuwenliang, Defect: 1170253 , begin
                downloadUA = mContext.getResources().getString(R.string.def_download_useragent);
                //downloadUA = "test";
                // fuwenliang, Defect: 1170253 , end
            }
            TctLog.i("TCTLOG", "sendAndRecevie downloadUA = " + downloadUA);

            if("1".equals(SystemProperties.get("ro.download.useragent.enable","0"))) { // default on
                TctLog.i("TCTLOG", "sendAndRecevie DEFAULT_USER_AGENT = " + Constants.DEFAULT_USER_AGENT);
                httpRequest.addHeader("User-Agent", Constants.DEFAULT_USER_AGENT);
            } else if (downloadUA != null && !downloadUA.equals("")) {
                httpRequest.addHeader("User-Agent", constructTctUserAgent(downloadUA));
                TctLog.i("TCTLOG", "sendAndRecevie constructTctUserAgent = " + constructTctUserAgent(downloadUA));
            }
//[BUGFIX]-Add-END by TCTNB.(JiangLong Pan)
//[FEATURE]-Mod-END by TCTNB.xihe.lu
            HttpResponse httpResponse = new DefaultHttpClient()
                    .execute(httpRequest);

            if (ODDefines.INSTALLNOTIFY_REPORT_DOWNLOAD_SUCCESS.equals(code)) { // install
                                                                                // success
                if (httpResponse.getStatusLine().getStatusCode() == 200) {// success
                    mStrResult = httpResponse.getStatusLine().toString();
                    TctLog.d("RequestAndResponseStatus", "ywp-strResult: "
                            + httpResponse.getStatusLine().toString());
                } else {
                    code = ODDefines.INSTALLNOTIFY_REPORT_DEVICE_ABORTED;
                    sendAndRecevie();
                    TctLog.d("RequestAndResponseStatus", "ywp-Error Response"
                            + httpResponse.getStatusLine().toString());
                }// end if
            }// end if
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e) {
             e.printStackTrace();
        }// end try
        return mStrResult;
    }

    private String uri;
    private String code;
}
